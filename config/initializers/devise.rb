# frozen_string_literal: true

Devise.setup do |config|
  require 'devise/orm/active_record'

  config.allow_unconfirmed_access_for = 1.hour
  config.case_insensitive_keys = [:email]
  config.email_regexp = /\A[^@\s]+@[^@\s]+\z/
  config.expire_all_remember_me_on_sign_out = true
  config.last_attempt_warning = true
  config.lock_strategy = :none
  config.mailer = 'DeviseMailer'
  config.mailer_sender = ENV['EMAIL']
  config.maximum_attempts = 5
  config.navigational_formats = []
  config.password_length = 6..32
  config.reconfirmable = true
  config.remember_for = 2.weeks
  config.reset_password_within = 6.hours
  config.secret_key = ENV['DEVISE_SECRET_KEY']
  config.sign_out_via = :delete
  config.skip_session_storage = %i[http_auth]
  config.stretches = Rails.env.test? ? 1 : 12
  config.strip_whitespace_keys = [:email]
  config.unlock_keys = [:email]
  config.unlock_strategy = :email
end
