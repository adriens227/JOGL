# frozen_string_literal: true

if LogStasher.enabled?
  LogStasher.add_custom_fields do |fields|
    fields[:key] = request.headers['X-API-KEY'] 
  end
end
