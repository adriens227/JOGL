# frozen_string_literal: true

if not %w[development test].include? Rails.env
  require 'prometheus_exporter/middleware'

  Rails.application.middleware.unshift PrometheusExporter::Middleware
end
