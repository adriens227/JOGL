# frozen_string_literal: true

Geocoder.configure(
  cache: Redis.new(Rails.configuration.redis_options),
  cache_options: {
    prefix: 'geocoder:'
  },
  http_headers: { 'User-Agent' => Rails.configuration.contact_email },
  timeout: 5,
  units: :km
)
