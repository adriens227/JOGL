# frozen_string_literal: true

require 'rack/cors'

Rails.application.config.middleware.insert_before 0, Rack::Cors do
  allow do
    origins '*'
    resource '*', headers: :any, expose: ["Authorization"], methods: %i[delete get options patch post put]
  end
end
