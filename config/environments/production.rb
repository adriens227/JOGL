# frozen_string_literal: true

require 'active_support/core_ext/integer/time'

REDIS_OPTIONS = {
  host: 'master',
  sentinels: [{ host: ENV['REDIS_SENTINEL_SERVICE_HOST'], port: ENV['REDIS_SENTINEL_SERVICE_PORT'] }],
  role: :master
}

Rails.application.configure do
  config.cache_classes = true
  config.cache_store = :redis_cache_store, REDIS_OPTIONS
  config.consider_all_requests_local = false
  config.eager_load = true

  config.graph_name = ENV['GRAPH_NAME']

  config.log_level = :info
  config.log_tags = [:request_id]

  config.redis_options = REDIS_OPTIONS

  config.search_cluster_name = ENV['SEARCH_CLUSTER_NAME']

  config.action_controller.perform_caching = true

  config.action_mailer.perform_caching = false

  config.action_policy.cache_store = :redis_cache_store, REDIS_OPTIONS

  config.active_record.dump_schema_after_migration = false

  config.active_storage.service = :digitalocean

  config.active_support.report_deprecations = false

  response = Faraday.get("http://#{ENV['LIBRETRANSLATE_SERVICE_HOST']}:#{ENV['LIBRETRANSLATE_SERVICE_PORT']}/languages")
  config.i18n.available_locales = JSON.parse(response.body).map { |language| language['code'].to_sym }
  config.i18n.fallbacks = true
end
