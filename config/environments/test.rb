# frozen_string_literal: true

require 'active_support/core_ext/integer/time'

Rails.application.routes.default_url_options[:host] = Rails.application.config.host

Rails.application.configure do
  config.cache_classes = true
  config.cache_store = :null_store
  config.consider_all_requests_local = true
  config.eager_load = false
  config.logger = nil

  config.redis_options = { host: ENV['REDIS_MASTER_SERVICE_HOST'], port: ENV['REDIS_MASTER_SERVICE_PORT'] }

  config.search_cluster_name = nil
  
  config.action_controller.perform_caching = false
  config.action_controller.allow_forgery_protection = false

  config.action_dispatch.show_exceptions = false

  config.action_mailer.default_url_options = { host: ENV['HOST'] || 'localhost', port: 3000 }
  config.action_mailer.delivery_method = :test
  config.action_mailer.perform_caching = false

  config.action_policy.cache_store = :null_store

  config.active_storage.service = :test

  config.active_support.deprecation = :stderr
  config.active_support.disallowed_deprecation = :raise
  config.active_support.disallowed_deprecation_warnings = []

  config.i18n.raise_on_missing_translations = true

  config.public_file_server.enabled = true
  config.public_file_server.headers = {
    'Cache-Control' => "public, max-age=#{1.hour.to_i}"
  }
end
