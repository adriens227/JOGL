# frozen_string_literal: true

# Load the Rails application.
require_relative 'application'

# initialize the Rails application.
Rails.application.initialize!
