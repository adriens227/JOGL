# frozen_string_literal: true

namespace :search do
  desc 'Truncate all Searchable indexes'
  task clear: :environment do
    Rails.application.eager_load!

    ApplicationRecord.descendants.each do |model|
      next unless model.include?(Searchable)

      model.index_truncate
    end
  end

  desc 'Drop all Searchable indexes'
  task drop: :environment do
    Rails.application.eager_load!

    ApplicationRecord.descendants.each do |model|
      next unless model.include?(Searchable)

      model.index_drop
    end
  end

  desc 'Reindex all Searchable indexes'
  task reindex: :environment do
    Rails.application.eager_load!

    Rake::Task['search:clear'].execute

    ApplicationRecord.descendants.each do |model|
      next unless model.include?(Searchable)

      model.all.each(&:index)
    end
  end

  desc 'Reset the search server by dropping, migrating, and reindexing'
  task reset: :environment do
    Rake::Task['search:drop'].execute
    Rake::Task['search:sql'].invoke('migrate')
    Rake::Task['search:sql'].invoke('cluster') if Rails.env.production?
    Rake::Task['search:reindex'].execute
  end

  desc 'Execute a SQL file on a search instance'
  task :sql, [:name] => :environment do |_, args|
    Rails.application.eager_load!

    file = Rails.root.join("db/manticore/#{args[:name]}.sql")
    directory = Rails.root.join('tmp')

    `cd #{directory} && csplit #{file} --suppress-matched -f '#{args[:name]}.' -b '%03d.sql' -z -s /---/ '{*}'`

    Dir.glob("#{directory}/#{args[:name]}.*.sql").each do |filename|
      file = File.open(filename)
      query = file.read
      file.close

      Search::CLIENT.query(query)

      File.delete(filename)
    end
  end
end
