# frozen_string_literal: true

require 'colorize'

namespace :graph do
  desc 'Clear out the existing graph data.'
  task clear: :environment do
    RedisGraph.new(Rails.configuration.graph_name, Rails.configuration.redis_options).delete
  rescue StandardError
    puts "No graph (#{Rails.configuration.graph_name}) to delete".colorize(:yellow)
  end

  desc 'Rebuild the underlying graph representation of JOGL data.'
  task rebuild: :environment do
    Rails.application.eager_load!

    Rake::Task['graph:clear'].execute

    ApplicationRecord.descendants.each do |model|
      next unless model.include?(Graphable)

      model.all.each do |obj|
        obj.merge_node
        obj.create_belongs_to_edges
        obj.create_has_many_edges
      end
    end

    User.all.each do |user|
      user.roles.each do |role|
        next if role.resource_type.nil? || role.resource_id.nil?

        GraphJob.perform_now('merge_edge', 'User', user.id, role.resource_type, role.resource_id)
      end
    end
  end
end
