FROM registry.digitalocean.com/jogl/base

ENV RAILS_ENV=production

COPY --chown=runner:runner config.ru /home/runner/
COPY --chown=runner:runner Gemfile* /home/runner/
COPY --chown=runner:runner Procfile /home/runner/
COPY --chown=runner:runner Rakefile /home/runner/

COPY --chown=runner:runner app /home/runner/app/
COPY --chown=runner:runner bin /home/runner/bin/
COPY --chown=runner:runner config /home/runner/config/
COPY --chown=runner:runner db /home/runner/db/
COPY --chown=runner:runner lib /home/runner/lib/
COPY --chown=runner:runner public /home/runner/public/

WORKDIR /home/runner

RUN bundle config set --local without 'development test'
RUN bundle config set --local path 'vendor/bundle'
RUN bundle config set --local deployment true

RUN bundle install --quiet

COPY --chown=runner:runner deploy/docker/rails/entrypoint /home/runner/
RUN chmod +x /home/runner/entrypoint

EXPOSE 3000

ENTRYPOINT ["/home/runner/entrypoint"]
