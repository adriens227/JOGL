FROM ubuntu:focal

ENV DEBIAN_FRONTEND=noninteractive

RUN apt update
RUN apt install -y autoconf automake build-essential cmake curl git libtool m4 peg python redis-tools ruby ruby-open4

RUN mkdir -p /build
WORKDIR /build

RUN curl -O https://download.redis.io/releases/redis-6.2.6.tar.gz
RUN tar xvf redis-6.2.6.tar.gz
RUN make -C redis-6.2.6

RUN git clone --recurse-submodules -j8 https://github.com/RedisGraph/RedisGraph.git
RUN make -C RedisGraph

RUN mkdir -p /tmp/redis

COPY deploy/docker/redis/redis.conf /build/
COPY deploy/docker/redis/sentinel.conf /build/

COPY deploy/docker/redis/entrypoint /build/
RUN chmod +x /build/entrypoint

EXPOSE 6379 16379 26379

ENTRYPOINT ["/build/entrypoint"]
