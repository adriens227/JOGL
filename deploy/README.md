KUBERNETES
==========

Create a namespace:

```
%> kubectl create namespace jogl
```

## LibreTranslate

```
%> kubectl apply -f deploy/k8s/libretranslate-pod.yml
%> kubectl apply -f deploy/k8s/libretranslate-service.yml
```
## Fluentd

```
%> kubectl apply -f deploy/k8s/fluentd-pod.yml
%> kubectl apply -f deploy/k8s/fluentd-service.yml
```

## Manticore

```
%> kubectl apply -f deploy/k8s/manticore-master-pvc.yml
%> kubectl apply -f deploy/k8s/manticore-master-pv.yml
%> kubectl apply -f deploy/k8s/manticore-master-pod.yml
%> kubectl apply -f deploy/k8s/manticore-master-service.yml
%> kubectl apply -f deploy/k8s/manticore-replica-deployment.yml
%> kubectl apply -f deploy/k8s/manticore-replica-service.yml
```

Connect to a Rails pod and run the following:

```
%> bundle exec rake search:sql[migrate]
%> bundle exec rake search:sql[cluster]
%> bundle exec rake search:reindex
```

## Redis

```
%> kubectl apply -f deploy/k8s/redis-master-deployment.yml
%> kubectl apply -f deploy/k8s/redis-master-service.yml
%> kubectl apply -f deploy/k8s/redis-sentinel-deployment.yml
%> kubectl apply -f deploy/k8s/redis-sentinel-service.yml
%> kubectl apply -f deploy/k8s/redis-replica-deployment.yml
```

Connect to a Rails pod and run the following:

```
%> bundle exec rake graph:rebuild
```

## Config

*NOTE* Secrets are not managed via version control.

```
%> kubectl apply -f deploy/k8s/rails-configmap-production.yml

%> kubectl apply -f rails-secrets-production.yml
```

## Rails

### Building

```
%> docker build -t jogl/rails -f deploy/docker/rails/Dockerfile .
%> docker tag jogl/rails registry.digitalocean.com/jogl/rails
%> docker push registry.digitalocean.com/jogl/rails
```

### Installation

```
%> kubectl apply -f deploy/k8s/rails-deployment.yml
%> kubectl apply -f deploy/k8s/rails-service.yml
%> kubectl -n jogl scale deployments/rails --replicas=3
```

### Re-deployment

You need to scale the number of replicas down to one to ensure that the migrations only run once.
You can then scale back up to three replicas.

```
%> kubectl -n jogl scale deployments/rails --replicas=1
%> kubectl -n jogl rollout restart deployments/rails
%> kubectl -n jogl scale deployments/rails --replicas=3
```

## Monitoring

```
%> kubectl apply -f deploy/k8s/monitoring/rails-service-monitor.yml 
%> helm upgrade kube-prometheus-stack prometheus-community/kube-prometheus-stack --namespace kube-prometheus-stack -f values.yml
```
