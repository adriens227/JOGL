# frozen_string_literal: true

class CreatePrograms < ActiveRecord::Migration[7.0]
  def change
    create_table :programs, id: :uuid do |t|
      t.uuid :space_id, foreign_key: true, index: true

      t.boolean :deleted, null: false, default: false, index: true
      t.string :description, length: 1024
      t.bigint :search_id
      t.string :slug, null: false, index: { unique: true }, length: 64
      t.string :summary, length: 512
      t.date :start
      t.date :stop
      t.string :title, null: false, length: 128
      t.integer :visibility, null: false, default: 0, index: true

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    add_index :programs, :slug, unique: true, name: 'unique_programs_slug_index'
  end
end
