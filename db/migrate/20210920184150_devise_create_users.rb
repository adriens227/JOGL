# frozen_string_literal: true

class DeviseCreateUsers < ActiveRecord::Migration[7.0]
  def change
    create_table :users, id: :uuid do |t|
      ## Authenticatable
      t.string :email, null: false
      t.string :encrypted_password, null: false

      ## Confirmable
      t.string   :confirmation_token
      t.datetime :confirmed_at
      t.datetime :confirmation_sent_at
      t.string   :unconfirmed_email # Only if using reconfirmable

      ## Recoverable
      t.string   :reset_password_token
      t.datetime :reset_password_sent_at

      ## Rememberable
      t.datetime :remember_created_at

      ## Trackable
      t.integer  :sign_in_count, null: false, default: 0
      t.datetime :current_sign_in_at
      t.datetime :last_sign_in_at
      t.string   :current_sign_in_ip
      t.string   :last_sign_in_ip

      ## Lockable
      t.integer  :failed_attempts, null: false, default: 0
      t.string   :unlock_token
      t.datetime :locked_at

      t.uuid :location_id, foreign_key: true

      t.integer :birth_year
      t.boolean :deleted, null: false, default: false
      t.string :description, limit: 1024
      t.bigint :gender
      t.string :first_name, null: false, limit: 32
      t.string :job_title, limit: 64
      t.string :last_name, null: false, limit: 32
      t.jsonb :options, null: false, default: {}
      t.string :phone_number, limit: 16
      t.string :profession, limit: 64
      t.bigint :search_id
      t.string :slug, null: false, limit: 64
      t.string :summary, limit: 512
      t.string :username, null: false, limit: 64

      t.integer :comments_count, null: false, default: 0
      t.integer :groups_count, null: false, default: 0
      t.integer :mentions_count, null: false, default: 0
      t.integer :notifications_count, null: false, default: 0
      t.integer :posts_count, null: false, default: 0
      t.integer :projects_count, null: false, default: 0
      t.integer :spaces_count, null: false, default: 0

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    add_index :users, :options, using: :gin
    add_index :users, :confirmation_token, unique: true, name: 'users_confirmation_token_index'
    add_index :users, :email, unique: true, name: 'users_email_index'
    add_index :users, :reset_password_token, unique: true, name: 'users_reset_password_token_index'
    add_index :users, :slug, unique: true, name: 'users_slug_index'
    add_index :users, :unlock_token, unique: true, name: 'users_unlock_token_index'
    add_index :users, :username, unique: true, name: 'users_username_index'
  end
end
