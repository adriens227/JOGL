# frozen_string_literal: true

class CreateSpaces < ActiveRecord::Migration[7.0]
  def change
    create_table :spaces, id: :uuid do |t|
      t.uuid :owner_id, null: false, foreign_key: { to_table: :users }, index: true
      t.uuid :space_id, foreign_key: { to_table: :spaces }, index: true

      t.integer :category, null: false, default: 0
      t.boolean :deleted, null: false, default: false
      t.string :description, limit: 1024
      t.jsonb :options, null: false, default: {}
      t.bigint :search_id
      t.string :slug, null: false, index: { unique: true }, limit: 64
      t.string :summary, limit: 512
      t.string :title, null: false, limit: 128
      t.integer :visibility, null: false, default: 0, index: true

      t.integer :programs_count, null: false, default: 0
      t.integer :projects_count, null: false, default: 0
      t.integer :spaces_count, null: false, default: 0

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    add_index :spaces, :options, using: :gin
  end
end
