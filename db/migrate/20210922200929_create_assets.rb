# frozen_string_literal: true

class CreateAssets < ActiveRecord::Migration[7.0]
  def change
    create_table :assets, id: :uuid do |t|
      t.string :name, null: false, index: { unique: true }, limit: 64
      t.bigint :search_id

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    create_table :assets_resources do |t|
      t.uuid :asset_id, null: false, foreign_key: true, index: true
      t.uuid :resource_id, null: false
      t.string :resource_type, null: false
    end

    add_index :assets_resources, %i[resource_id resource_type], name: 'assets_resource_index'
    add_index :assets_resources, %i[asset_id resource_id resource_type], unique: true, name: 'unique_asset_resource_index'
  end
end
