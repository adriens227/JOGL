# frozen_string_literal: true

class CreateMoments < ActiveRecord::Migration[7.0]
  def change
    create_table :moments, id: :uuid do |t|
      t.uuid :phase_id, null: false, foreign_key: true, index: true

      t.date :date, null: false
      t.string :description, limit: 1024
      t.string :title, limit: 128

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
