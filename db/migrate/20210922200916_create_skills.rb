# frozen_string_literal: true

class CreateSkills < ActiveRecord::Migration[7.0]
  def change
    create_table :skills, id: :uuid do |t|
      t.string :name, null: false, index: { unique: true }, limit: 64
      t.bigint :search_id
      t.string :slug, null: false, index: { unique: true }, limit: 64

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    create_table :skills_resources do |t|
      t.uuid :resource_id, null: false
      t.string :resource_type, null: false
      t.uuid :skill_id, null: false, foreign_key: true, index: true
    end

    add_index :skills_resources, %i[resource_id resource_type], name: 'skills_resource_index'
    add_index :skills_resources, %i[resource_id resource_type skill_id], unique: true, name: 'unique_skill_resource_index'
  end
end
