# frozen_string_literal: true

class CreateLanguages < ActiveRecord::Migration[7.0]
  def change
    create_table :languages, id: false do |t|
      t.string :code, null: false, primary_key: true, limit: 2

      t.string :name, null: false, index: { unique: true }, limit: 96
      t.bigint :search_id
      t.boolean :supported, null: false, default: false

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
