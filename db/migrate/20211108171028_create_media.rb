# frozen_string_literal: true

class CreateMedia < ActiveRecord::Migration[7.0]
  def change
    create_table :media, id: :uuid do |t|
      t.uuid :resource_id, null: false
      t.string :resource_type, null: false

      t.boolean :deleted, null: false, default: false

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    add_index :media, %i[resource_id resource_type], name: 'unique_medias_resource_index'
  end
end
