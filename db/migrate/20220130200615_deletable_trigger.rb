# frozen_string_literal: true

class DeletableTrigger < ActiveRecord::Migration[7.0]
  def up
    execute <<-SQL.squish
      CREATE OR REPLACE FUNCTION soft_delete()
        RETURNS trigger AS $$
          DECLARE
            command text := ' SET deleted = true WHERE id = $1';
          BEGIN
            EXECUTE 'UPDATE ' || TG_TABLE_NAME || command USING OLD.id;
            RETURN NULL;
          END;
        $$ LANGUAGE plpgsql;

      DO $$
      DECLARE
        t text;
      BEGIN
        FOR t IN
          SELECT table_name FROM information_schema.columns WHERE column_name = 'deleted'
        LOOP
          EXECUTE format('CREATE TRIGGER soft_delete
                          BEFORE DELETE ON %I
                          FOR EACH ROW EXECUTE PROCEDURE soft_delete()', t, t);
        END LOOP;
      END;
      $$ LANGUAGE plpgsql;
    SQL
  end

  def down
    execute <<-SQL.squish
      DO $$
      DECLARE
        t text;
      BEGIN
        FOR t IN
          SELECT table_name FROM information_schema.columns WHERE column_name = 'deleted'
        LOOP
          EXECUTE format('DROP TRIGGER IF EXISTS soft_delete ON %I', t, t);

        END LOOP;
      END;
      $$ LANGUAGE plpgsql;

      DROP FUNCTION IF EXISTS soft_delete();
    SQL
  end
end
