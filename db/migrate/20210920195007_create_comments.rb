# frozen_string_literal: true

class CreateComments < ActiveRecord::Migration[7.0]
  def change
    create_table :comments, id: :uuid do |t|
      t.uuid :owner_id, null: false, foreign_key: { to_table: :users }, index: true
      t.uuid :post_id, null: false, foreign_key: true, index: true

      t.string :content, null: false, limit: 1024
      t.boolean :deleted, null: false, default: false
      t.bigint :search_id

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
