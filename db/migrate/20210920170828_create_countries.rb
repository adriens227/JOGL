# frozen_string_literal: true

class CreateCountries < ActiveRecord::Migration[7.0]
  def change
    create_table :countries, id: false do |t|
      t.string :code, null: false, primary_key: true, limit: 2

      t.string :name, null: false, index: { unique: true }, limit: 64
      t.bigint :search_id

      t.integer :divisions_count, null: false, default: 0

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
