# frozen_string_literal: true

class CreateAddresses < ActiveRecord::Migration[7.0]
  def change
    create_table :addresses, id: :uuid do |t|
      t.string :country_code, null: false, foreign_key: { to_table: :countries, column: :code }, index: true, limit: 2
      t.string :division_code, null: false, foreign_key: { to_table: :divisions, column: :code }, index: true, limit: 6

      t.string :city_locality, limit: 32
      t.string :postal_code, limit: 16
      t.string :street, limit: 64
    end

    add_foreign_key :addresses, :countries, column: :country_code, primary_key: :code
    add_foreign_key :addresses, :divisions, column: :division_code, primary_key: :code
  end
end
