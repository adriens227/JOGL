# frozen_string_literal: true

class CreateKeys < ActiveRecord::Migration[7.0]
  create_table :keys, id: :uuid do |t|
    t.uuid :resource_id, null: false
    t.string :resource_type, null: false

    t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
  end
end
