# frozen_string_literal: true

class CreateRoles < ActiveRecord::Migration[7.0]
  def change
    create_table :roles, id: :uuid do |t|
      t.uuid :resource_id
      t.string :resource_type
      t.uuid :user_id, null: false

      t.string :name, null: false, limit: 32

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    add_index :roles, %i[resource_id resource_type]
    add_index :roles, %i[resource_id resource_type user_id], unique: true
  end
end
