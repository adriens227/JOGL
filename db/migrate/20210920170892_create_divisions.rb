# frozen_string_literal: true

class CreateDivisions < ActiveRecord::Migration[7.0]
  def change
    create_table :divisions, id: false do |t|
      t.string :code, null: false, primary_key: true, limit: 7

      t.string :country_code, null: false, index: true, limit: 2

      t.string :name, null: false, limit: 128, index: true
      t.bigint :search_id

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    add_foreign_key :divisions, :countries, column: :country_code, primary_key: :code
  end
end
