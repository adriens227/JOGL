# frozen_string_literal: true

class CreateActivities < ActiveRecord::Migration[7.0]
  def change
    create_table :activities, id: :uuid do |t|
      t.uuid :activity_id, foreign_key: true, index: true
      t.uuid :location_id, foreign_key: true, index: true
      t.uuid :resource_id, null: false
      t.string :resource_type, null: false

      t.datetime :deadline
      t.string :description, limit: 1024
      t.string :kind, null: false, limit: 32
      t.jsonb :options, null: false, default: {}
      t.bigint :search_id
      t.string :slug, null: false, limit: 64
      t.datetime :start
      t.datetime :stop
      t.string :summary, limit: 512
      t.string :title, null: false, limit: 128
      t.integer :visibility, null: false, default: 0, index: true

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    add_index :activities, :options, using: :gin
  end
end
