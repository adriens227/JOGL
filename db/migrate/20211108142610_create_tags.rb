class CreateTags < ActiveRecord::Migration[7.0]
  def change
    create_table :tags, id: :uuid do |t|
      t.string :name, null: false, index: { unique: true }, limit: 64
      t.bigint :search_id

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    create_table :tags_resources do |t|
      t.uuid :resource_id, null: false
      t.string :resource_type, null: false
      t.uuid :tag_id, null: false, foreign_key: true, index: true
    end

    add_index :tags_resources, %i[resource_id resource_type], name: 'tags_resources_index'
    add_index :tags_resources, %i[resource_id resource_type tag_id], unique: true, name: 'unique_tag_resource_index'
  end
end
