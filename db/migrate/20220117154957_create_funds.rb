class CreateFunds < ActiveRecord::Migration[7.0]
  def change
    create_table :funds, id: :uuid do |t|
      t.uuid :resource_id, null: false
      t.string :resource_type, null: false

      t.float :total, null: false, default: 0
      t.float :maximum_disbursement

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
