# frozen_string_literal: true

class CreateMentions < ActiveRecord::Migration[7.0]
  def change
    create_table :mentions, id: :uuid do |t|
      t.uuid :post_id, null: false, foreign_key: true, index: true
      t.uuid :resource_id, null: false
      t.string :resource_type, null: false

      t.boolean :deleted, null: false, default: false

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end

    add_index :mentions, %i[resource_id resource_type], name: 'mentions_resource_index'
    add_index :mentions, %i[post_id resource_id resource_type], unique: true, name: 'unique_mention_index'
  end
end
