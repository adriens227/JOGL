# frozen_string_literal: true

class CreatePhases < ActiveRecord::Migration[7.0]
  def change
    create_table :phases, id: :uuid do |t|
      t.uuid :project_id, null: false, foreign_key: true, index: true

      t.string :color, null: false, default: '000000', limit: 6
      t.boolean :deleted, null: false, default: false
      t.date :start, null: false
      t.date :stop, null: false
      t.string :title, null: false, limit: 128

      t.timestamps null: false, default: -> { 'CURRENT_TIMESTAMP' }
    end
  end
end
