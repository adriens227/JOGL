# frozen_string_literal: true

after 'development:projects' do
  [0, 1, 2].each do |n|
    Project.first.text_sections << TextSection.new(category: TextSection.categories.key(n),
                                                   content: FFaker::CheesyLingo.paragraph[0..511],
                                                   title: FFaker::CheesyLingo.title[0..127])
  end
end
