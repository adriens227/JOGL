# frozen_string_literal: true

after 'development:addresses' do
  Location.create!(address: Address.first,
                   latitude: FFaker::Geolocation.lat,
                   longitude: FFaker::Geolocation.lng,
                   name: FFaker::Venue.name[0..63])
end
