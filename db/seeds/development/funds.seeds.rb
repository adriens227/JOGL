# frozen_string_literal: true

after 'development:activities' do
  Fund.create(maximum_disbursement: 1_000.00,
              resource: PeerReview.first,
              total: 10_000.00)
end
