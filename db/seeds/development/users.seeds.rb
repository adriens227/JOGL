# frozen_string_literal: true

after 'development:languages', 'development:locations' do
  return unless User.count.zero?

  first_name = FFaker::Name.first_name
  last_name = FFaker::Name.last_name

  domain = FFaker::Internet.domain_name
  email = "#{first_name}.#{last_name}@#{domain}"

  password = (1..2).collect { |_| FFaker::DizzleIpsum.word.gsub(/[^A-Za-z0-9]+/, '').downcase }.join[0..31]

  user = User.create!(birth_year: Time.zone.now.year - 25,
                      confirmed_at: Time.zone.now,
                      description: FFaker::DizzleIpsum.paragraph[0..127],
                      email: email,
                      first_name: first_name,
                      last_name: last_name,
                      location: Location.first,
                      password: password,
                      password_confirmation: password)

  puts "USER       (id) #{user.id.colorize(:cyan)} (slug) #{user.slug.colorize(:cyan)}"
  puts "           (key) #{user.key.id.colorize(:cyan)} (email) #{user.email.colorize(:cyan)} (password) #{user.password.colorize(:cyan)}"
end
