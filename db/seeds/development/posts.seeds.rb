# frozen_string_literal: true

after 'development:users' do
  post = Post.new(content: FFaker::CheesyLingo.paragraph[0..255], owner: User.first)

  Feed.first.posts << post
end
