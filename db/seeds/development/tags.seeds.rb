# frozen_string_literal: true

after 'development:users', 'development:spaces' do
  tag = Tag.create!(name: 'FindTheCure')

  User.first.tags << tag
  Space.first.tags << tag
end
