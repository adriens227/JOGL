# frozen_string_literal: true

after do
  Country.create!(code: 'FR', name: 'France')
  Division.create!(code: 'FR-75', country_code: 'FR', name: 'Paris')
end
