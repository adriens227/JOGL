# frozen_string_literal: true

after 'development:spaces' do
  program = Program.create!(description: FFaker::CheesyLingo.sentence[0..511],
                            space: Space.first,
                            start: Time.zone.today,
                            stop: Time.zone.tomorrow,
                            title: FFaker::CheesyLingo.title[0..127])

  puts "PROGRAM    (id) #{program.id.colorize(:cyan)} (slug) #{program.slug.colorize(:cyan)}"
end
