# frozen_string_literal: true

after 'development:spaces' do
  challenge = Challenge.create(deadline: Time.zone.now,
                               resource: Space.first,
                               start: Time.zone.now - 1.day,
                               stop: Time.zone.now + 1.day,
                               title: FFaker::CheesyLingo.title[0..127])

  puts "CHALLENGE  (id) #{challenge.id.colorize(:cyan)} (slug) #{challenge.slug.colorize(:cyan)}"
  
  peer_review = PeerReview.create(deadline: Time.zone.now,
                                  resource: challenge,
                                  start: Time.zone.now - 1.day,
                                  stop: Time.zone.now + 1.day,
                                  title: FFaker::CheesyLingo.title[0..127])

  puts "PEERREVIEW (id) #{peer_review.id.colorize(:cyan)} (slug) #{challenge.slug.colorize(:cyan)}"
end
