# frozen_string_literal: true

after 'development:users', 'development:posts' do
  mention = Mention.new(resource: User.first)

  Post.first.mentions << mention
end
