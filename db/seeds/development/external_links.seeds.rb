# frozen_string_literal: true

after 'development:users', 'development:spaces' do
  ExternalLink.create!(resource: Space.first.media, url: FFaker::Internet.http_url)

  ExternalLink.create!(resource: User.first.media, text: FFaker::CheesyLingo.title[0..127], url: FFaker::Internet.http_url)
end
