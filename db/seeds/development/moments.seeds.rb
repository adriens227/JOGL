# frozen_string_literal: true

after 'development:phases' do
  Moment.create!(date: Time.zone.tomorrow,
                 phase: Phase.first,
                 title: FFaker::CheesyLingo.title[0..127])
end
