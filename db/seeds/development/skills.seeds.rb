# frozen_string_literal: true

after 'development:users', 'development:activities' do
  skill = Skill.new(name: FFaker::Skill.specialty[0..63])

  Activity.first.skills << skill

  User.first.skills << skill
end
