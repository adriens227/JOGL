# frozen_string_literal: true

require 'json'

file = File.read(Rails.root.join('db/seeds/countries_divisions.json'))

data = JSON.parse(file)

data.each do |key, value|
  country = Country.find_or_create_by(code: key, name: value['name'])

  value['divisions'].each do |key, value|
    Division.find_or_create_by(code: key, country_code: country.code, name: value)
  end
end
