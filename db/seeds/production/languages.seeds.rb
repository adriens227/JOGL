# frozen_string_literal: true

require 'csv'
require 'json'

file = File.read(Rails.root.join('db/seeds/languages.csv'))

data = CSV.parse(file)

data.each do |row|
  Language.find_or_create_by(code: row.first.upcase, name: row.second)
end

response = Faraday.get("http://#{ENV['LIBRETRANSLATE_SERVICE_HOST']}:#{ENV['LIBRETRANSLATE_SERVICE_PORT']}/languages")

if response.status == 200
  languages = JSON.parse(response.body)

  languages.each do |language|
    code = language['code'].upcase

    Language.find_by(code: code).update_column('supported', true)
  end
end
