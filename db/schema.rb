# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `bin/rails
# db:schema:load`. When creating a new database, `bin/rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema[7.0].define(version: 2022_01_30_200615) do
  # These are extensions that must be enabled in order to support this database
  enable_extension "pgcrypto"
  enable_extension "plpgsql"

  create_table "active_storage_attachments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.uuid "record_id", null: false
    t.uuid "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.string "service_name", null: false
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "active_storage_variant_records", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "blob_id", null: false
    t.string "variation_digest", null: false
    t.index ["blob_id", "variation_digest"], name: "index_active_storage_variant_records_uniqueness", unique: true
  end

  create_table "activities", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "activity_id"
    t.uuid "location_id"
    t.uuid "resource_id", null: false
    t.string "resource_type", null: false
    t.datetime "deadline"
    t.string "description", limit: 1024
    t.string "kind", limit: 32, null: false
    t.jsonb "options", default: {}, null: false
    t.bigint "search_id"
    t.string "slug", limit: 64, null: false
    t.datetime "start"
    t.datetime "stop"
    t.string "summary", limit: 512
    t.string "title", limit: 128, null: false
    t.integer "visibility", default: 0, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["activity_id"], name: "index_activities_on_activity_id"
    t.index ["location_id"], name: "index_activities_on_location_id"
    t.index ["options"], name: "index_activities_on_options", using: :gin
    t.index ["visibility"], name: "index_activities_on_visibility"
  end

  create_table "addresses", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "country_code", limit: 2, null: false
    t.string "division_code", limit: 6, null: false
    t.string "city_locality", limit: 32
    t.string "postal_code", limit: 16
    t.string "street", limit: 64
    t.index ["country_code"], name: "index_addresses_on_country_code"
    t.index ["division_code"], name: "index_addresses_on_division_code"
  end

  create_table "applications", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "peer_review_id", null: false
    t.uuid "project_id", null: false
    t.integer "score"
    t.datetime "created_at", precision: 6, default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", precision: 6, default: -> { "CURRENT_TIMESTAMP" }, null: false
  end

  create_table "assets", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", limit: 64, null: false
    t.bigint "search_id"
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["name"], name: "index_assets_on_name", unique: true
  end

  create_table "assets_resources", force: :cascade do |t|
    t.uuid "asset_id", null: false
    t.uuid "resource_id", null: false
    t.string "resource_type", null: false
    t.index ["asset_id", "resource_id", "resource_type"], name: "unique_asset_resource_index", unique: true
    t.index ["asset_id"], name: "index_assets_resources_on_asset_id"
    t.index ["resource_id", "resource_type"], name: "assets_resource_index"
  end

  create_table "challenges_projects", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "challenge_id", null: false
    t.uuid "project_id", null: false
    t.boolean "accepted", default: false, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["challenge_id"], name: "index_challenges_projects_on_challenge_id"
    t.index ["project_id"], name: "index_challenges_projects_on_project_id"
  end

  create_table "comments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "owner_id", null: false
    t.uuid "post_id", null: false
    t.string "content", limit: 1024, null: false
    t.boolean "deleted", default: false, null: false
    t.bigint "search_id"
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["owner_id"], name: "index_comments_on_owner_id"
    t.index ["post_id"], name: "index_comments_on_post_id"
  end

  create_table "countries", primary_key: "code", id: { type: :string, limit: 2 }, force: :cascade do |t|
    t.string "name", limit: 64, null: false
    t.bigint "search_id"
    t.integer "divisions_count", default: 0, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["name"], name: "index_countries_on_name", unique: true
  end

  create_table "divisions", primary_key: "code", id: { type: :string, limit: 7 }, force: :cascade do |t|
    t.string "country_code", limit: 2, null: false
    t.string "name", limit: 128, null: false
    t.bigint "search_id"
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["country_code"], name: "index_divisions_on_country_code"
    t.index ["name"], name: "index_divisions_on_name"
  end

  create_table "external_links", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "resource_id", null: false
    t.string "resource_type", null: false
    t.string "text", limit: 128, null: false
    t.string "url", limit: 128, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
  end

  create_table "feeds", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "resource_id", null: false
    t.string "resource_type", null: false
    t.boolean "deleted", default: false, null: false
    t.jsonb "options", default: {}, null: false
    t.integer "posts_count", default: 0, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["options"], name: "index_feeds_on_options", using: :gin
    t.index ["resource_id", "resource_type"], name: "unique_feeds_resource_index", unique: true
  end

  create_table "funds", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "resource_id", null: false
    t.string "resource_type", null: false
    t.float "total", default: 0.0, null: false
    t.float "maximum_disbursement"
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
  end

  create_table "groups", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "resource_id", null: false
    t.string "resource_type", null: false
    t.string "name", limit: 64
    t.integer "users_count", default: 0, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["name"], name: "index_groups_on_name"
    t.index ["resource_id", "resource_type"], name: "groups_resource_index"
  end

  create_table "keys", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "resource_id", null: false
    t.string "resource_type", null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
  end

  create_table "languages", primary_key: "code", id: { type: :string, limit: 2 }, force: :cascade do |t|
    t.string "name", limit: 96, null: false
    t.bigint "search_id"
    t.boolean "supported", default: false, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["name"], name: "index_languages_on_name", unique: true
  end

  create_table "locations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "address_id"
    t.float "latitude"
    t.float "longitude"
    t.string "name", limit: 64
    t.bigint "search_id"
    t.index ["address_id"], name: "index_locations_on_address_id"
    t.index ["name"], name: "index_locations_on_name"
  end

  create_table "media", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "resource_id", null: false
    t.string "resource_type", null: false
    t.boolean "deleted", default: false, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["resource_id", "resource_type"], name: "unique_medias_resource_index"
  end

  create_table "mentions", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "post_id", null: false
    t.uuid "resource_id", null: false
    t.string "resource_type", null: false
    t.boolean "deleted", default: false, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["post_id", "resource_id", "resource_type"], name: "unique_mention_index", unique: true
    t.index ["post_id"], name: "index_mentions_on_post_id"
    t.index ["resource_id", "resource_type"], name: "mentions_resource_index"
  end

  create_table "moments", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "phase_id", null: false
    t.date "date", null: false
    t.string "description", limit: 1024
    t.string "title", limit: 128
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["phase_id"], name: "index_moments_on_phase_id"
  end

  create_table "notifications", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "recipient_id", null: false
    t.string "recipient_type", null: false
    t.jsonb "params"
    t.string "type", null: false
    t.datetime "read_at"
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["params"], name: "index_notifications_on_params", using: :gin
    t.index ["read_at"], name: "notifications_read_at_index"
  end

  create_table "phases", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "project_id", null: false
    t.string "color", limit: 6, default: "000000", null: false
    t.boolean "deleted", default: false, null: false
    t.date "start", null: false
    t.date "stop", null: false
    t.string "title", limit: 128, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["project_id"], name: "index_phases_on_project_id"
  end

  create_table "posts", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "feed_id", null: false
    t.uuid "owner_id", null: false
    t.integer "category", default: 0, null: false
    t.string "content", limit: 1024, null: false
    t.boolean "deleted", default: false, null: false
    t.bigint "search_id"
    t.integer "comments_count", default: 0, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["feed_id"], name: "index_posts_on_feed_id"
    t.index ["owner_id"], name: "index_posts_on_owner_id"
  end

  create_table "programs", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "space_id"
    t.boolean "deleted", default: false, null: false
    t.string "description"
    t.bigint "search_id"
    t.string "slug", null: false
    t.string "summary"
    t.date "start"
    t.date "stop"
    t.string "title", null: false
    t.integer "visibility", default: 0, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["deleted"], name: "index_programs_on_deleted"
    t.index ["slug"], name: "index_programs_on_slug", unique: true
    t.index ["slug"], name: "unique_programs_slug_index", unique: true
    t.index ["space_id"], name: "index_programs_on_space_id"
    t.index ["visibility"], name: "index_programs_on_visibility"
  end

  create_table "projects", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "location_id"
    t.uuid "owner_id", null: false
    t.uuid "space_id"
    t.boolean "deleted", default: false, null: false
    t.string "description", limit: 1024, null: false
    t.bigint "search_id"
    t.string "slug", limit: 64, null: false
    t.string "summary", limit: 512
    t.string "title", limit: 128, null: false
    t.integer "visibility", default: 0, null: false
    t.integer "moments_count", default: 0, null: false
    t.integer "phases_count", default: 0, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["deleted"], name: "index_projects_on_deleted"
    t.index ["location_id"], name: "index_projects_on_location_id"
    t.index ["owner_id"], name: "index_projects_on_owner_id"
    t.index ["slug"], name: "index_projects_on_slug", unique: true
    t.index ["space_id"], name: "index_projects_on_space_id"
    t.index ["visibility"], name: "index_projects_on_visibility"
  end

  create_table "proposals", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "peer_review_id", null: false
    t.uuid "project_id", null: false
    t.integer "score"
    t.string "title", limit: 128, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
  end

  create_table "roles", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "resource_id"
    t.string "resource_type"
    t.uuid "user_id", null: false
    t.string "name", limit: 32, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["resource_id", "resource_type", "user_id"], name: "index_roles_on_resource_id_and_resource_type_and_user_id", unique: true
    t.index ["resource_id", "resource_type"], name: "index_roles_on_resource_id_and_resource_type"
  end

  create_table "skills", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", limit: 64, null: false
    t.bigint "search_id"
    t.string "slug", limit: 64, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["name"], name: "index_skills_on_name", unique: true
    t.index ["slug"], name: "index_skills_on_slug", unique: true
  end

  create_table "skills_resources", force: :cascade do |t|
    t.uuid "resource_id", null: false
    t.string "resource_type", null: false
    t.uuid "skill_id", null: false
    t.index ["resource_id", "resource_type", "skill_id"], name: "unique_skill_resource_index", unique: true
    t.index ["resource_id", "resource_type"], name: "skills_resource_index"
    t.index ["skill_id"], name: "index_skills_resources_on_skill_id"
  end

  create_table "spaces", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "owner_id", null: false
    t.uuid "space_id"
    t.integer "category", default: 0, null: false
    t.boolean "deleted", default: false, null: false
    t.string "description", limit: 1024
    t.jsonb "options", default: {}, null: false
    t.bigint "search_id"
    t.string "slug", limit: 64, null: false
    t.string "summary", limit: 512
    t.string "title", limit: 128, null: false
    t.integer "visibility", default: 0, null: false
    t.integer "programs_count", default: 0, null: false
    t.integer "projects_count", default: 0, null: false
    t.integer "spaces_count", default: 0, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["options"], name: "index_spaces_on_options", using: :gin
    t.index ["owner_id"], name: "index_spaces_on_owner_id"
    t.index ["slug"], name: "index_spaces_on_slug", unique: true
    t.index ["space_id"], name: "index_spaces_on_space_id"
    t.index ["visibility"], name: "index_spaces_on_visibility"
  end

  create_table "sustainable_development_goals", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "color", limit: 6, default: "000000", null: false
    t.string "name", limit: 64, null: false
    t.integer "number", null: false
    t.bigint "search_id"
    t.index ["name"], name: "index_sustainable_development_goals_on_name", unique: true
    t.index ["number"], name: "index_sustainable_development_goals_on_number", unique: true
  end

  create_table "sustainable_development_goals_resources", force: :cascade do |t|
    t.uuid "sustainable_development_goal_id", null: false
    t.uuid "resource_id", null: false
    t.string "resource_type", null: false
    t.index ["resource_id", "resource_type"], name: "sustainable_development_goals_resource_index"
    t.index ["sustainable_development_goal_id", "resource_id", "resource_type"], name: "unique_sustainable_development_goals_resource_index", unique: true
    t.index ["sustainable_development_goal_id"], name: "sustainable_development_goals_resources_index"
  end

  create_table "tags", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "name", limit: 64, null: false
    t.bigint "search_id"
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["name"], name: "index_tags_on_name", unique: true
  end

  create_table "tags_resources", force: :cascade do |t|
    t.uuid "resource_id", null: false
    t.string "resource_type", null: false
    t.uuid "tag_id", null: false
    t.index ["resource_id", "resource_type", "tag_id"], name: "unique_tag_resource_index", unique: true
    t.index ["resource_id", "resource_type"], name: "tags_resources_index"
    t.index ["tag_id"], name: "index_tags_resources_on_tag_id"
  end

  create_table "text_sections", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "resource_id", null: false
    t.string "resource_type", null: false
    t.integer "category", default: 0, null: false
    t.string "content", limit: 1024
    t.boolean "deleted", default: false, null: false
    t.integer "position", default: 0, null: false
    t.bigint "search_id"
    t.string "title", limit: 128, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["resource_id", "resource_type"], name: "text_sections_resource_index"
  end

  create_table "translations", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.uuid "resource_id", null: false
    t.string "resource_type", null: false
    t.string "content", null: false
    t.boolean "edited", default: false, null: false
    t.string "field", null: false
    t.string "language_code", limit: 2, null: false
    t.bigint "search_id"
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["field", "language_code", "resource_id", "resource_type"], name: "unique_translation_index", unique: true
    t.index ["field", "resource_id", "resource_type"], name: "translation_field_index"
    t.index ["language_code", "resource_id", "resource_type"], name: "translation_language_index"
    t.index ["resource_id", "resource_type"], name: "index_translations_on_resource_id_and_resource_type"
  end

  create_table "users", id: :uuid, default: -> { "gen_random_uuid()" }, force: :cascade do |t|
    t.string "email", null: false
    t.string "encrypted_password", null: false
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.integer "failed_attempts", default: 0, null: false
    t.string "unlock_token"
    t.datetime "locked_at"
    t.uuid "location_id"
    t.integer "birth_year"
    t.boolean "deleted", default: false, null: false
    t.string "description", limit: 1024
    t.bigint "gender"
    t.string "first_name", limit: 32, null: false
    t.string "job_title", limit: 64
    t.string "last_name", limit: 32, null: false
    t.jsonb "options", default: {}, null: false
    t.string "phone_number", limit: 16
    t.string "profession", limit: 64
    t.bigint "search_id"
    t.string "slug", limit: 64, null: false
    t.string "summary", limit: 512
    t.string "username", limit: 64, null: false
    t.integer "comments_count", default: 0, null: false
    t.integer "groups_count", default: 0, null: false
    t.integer "mentions_count", default: 0, null: false
    t.integer "notifications_count", default: 0, null: false
    t.integer "posts_count", default: 0, null: false
    t.integer "projects_count", default: 0, null: false
    t.integer "spaces_count", default: 0, null: false
    t.datetime "created_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.datetime "updated_at", default: -> { "CURRENT_TIMESTAMP" }, null: false
    t.index ["confirmation_token"], name: "users_confirmation_token_index", unique: true
    t.index ["email"], name: "users_email_index", unique: true
    t.index ["options"], name: "index_users_on_options", using: :gin
    t.index ["reset_password_token"], name: "users_reset_password_token_index", unique: true
    t.index ["slug"], name: "users_slug_index", unique: true
    t.index ["unlock_token"], name: "users_unlock_token_index", unique: true
    t.index ["username"], name: "users_username_index", unique: true
  end

  create_table "users_groups", id: false, force: :cascade do |t|
    t.uuid "group_id", null: false
    t.uuid "user_id", null: false
    t.index ["group_id", "user_id"], name: "unique_user_group_index", unique: true
    t.index ["group_id"], name: "index_users_groups_on_group_id"
    t.index ["user_id"], name: "index_users_groups_on_user_id"
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
  add_foreign_key "active_storage_variant_records", "active_storage_blobs", column: "blob_id"
  add_foreign_key "addresses", "countries", column: "country_code", primary_key: "code"
  add_foreign_key "addresses", "divisions", column: "division_code", primary_key: "code"
  add_foreign_key "divisions", "countries", column: "country_code", primary_key: "code"
end
