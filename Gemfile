# frozen_string_literal: true

source 'https://rubygems.org'
git_source(:github) { |repo| "https://github.com/#{repo}.git" }

ruby '3.0.2'

gem 'action_policy', '0.6.0'
gem 'allowable', '1.1.0'
gem 'ar_lazy_preload', '1.0.0'
gem 'aws-sdk-s3', '1.112.0'
gem 'bcrypt', '3.1.16'
gem 'blueprinter', '0.25.3'
gem 'colorize', '0.8.1'
gem 'counter_culture', '3.2.0'
gem 'devise', '4.8.1'
gem 'devise-async', '1.0.0'
gem 'faraday_middleware', '1.2.0'
gem 'foreman', '0.87.2'
gem 'geocoder', '1.7.3'
gem 'image_processing', '1.12.1'
gem 'logstasher', '2.1.5'
gem 'mini_magick', '4.11.0'
gem 'mysql2', '0.5.3', platforms: %i[mri]
gem 'nokogiri', '1.13.2'
gem 'noticed', '1.5.7'
gem 'open4', '1.3.4'
gem 'pg', '1.3.1', platforms: %i[mri]
gem 'prometheus_exporter', '1.0.1'
gem 'puma', '5.6.2'
gem 'rack-cors', '1.1.1'
gem 'rails', '7.0.2.2'
gem 'rails-i18n', '7.0.1'
gem 'redis', '4.6.0'
gem 'redisgraph', '2.0.3'
gem 'rswag-api', '2.5.0'
gem 'rswag-ui', '2.5.0'
gem 'seedbank', '0.5.0'
gem 'sucker_punch', '3.0.1'
gem 'tzinfo-data', '1.2021.5', platforms: %i[jruby mingw mswin x64_mingw]
gem 'validate_url', '1.0.13'

group :development do
  gem 'brakeman', '5.2.1'
  gem 'bullet', '7.0.1'
  gem 'bundler-audit', '0.9.0.1'
  gem 'redcarpet', '3.5.1'
  gem 'rubocop', '1.25.1'
  gem 'rubocop-performance', '1.13.2'
  gem 'rubocop-rails', '2.13.2'
  gem 'rubocop-rake', '0.6.0'
  gem 'rubocop-rspec', '2.8.0'
  gem 'solargraph', '0.44.3'
  gem 'solargraph-rails', '0.3.1'
  gem 'yard', '0.9.27'
end

group :development, :test do
  gem 'byebug', '11.1.3', platforms: %i[mri mingw x64_mingw]
  gem 'dotenv-rails', '2.7.6'
  gem 'factory_bot_rails', '6.2.0'
  gem 'ffaker', '2.20.0'
  gem 'pry', '0.13.1'
  gem 'pry-byebug', '3.9.0'
  gem 'pry-rails', '0.3.9'
  gem 'rspec-rails', '5.1.0'
  gem 'rswag-specs', '2.5.0'
end

group :test do
  gem 'database_cleaner-active_record', '2.0.1'
  gem 'shoulda-matchers', '5.1.0'
  gem 'simplecov', '0.21.2', require: false
  gem 'sinatra', '2.1.0'
  gem 'webmock', '3.14.0'
end
