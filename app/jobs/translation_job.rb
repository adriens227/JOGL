# frozen_string_literal: true

class TranslationJob < ApplicationJob
  queue_as :default

  BASE_URI = "http://#{ENV['LIBRETRANSLATE_SERVICE_HOST']}:#{ENV['LIBRETRANSLATE_SERVICE_PORT']}".freeze

  def perform(klass, id, fields)
    instance = klass.constantize.find(id)

    return if instance.nil?

    fields.each do |field|
      content = instance.method(field).call

      next if content.nil?

      from = detect(content)

      translation = Translation.find_or_initialize_by(field: field, language: from, resource_id: id, resource_type: klass)

      unless translation.edited?
        translation.content = content
        translation.edited = true
        translation.save!
      end

      translate_all_languages(translation)
    end
  end

  private

  def detect(content)
    response = Http.json.post("#{BASE_URI}/detect", q: content)

    return nil if response.status != 200

    return nil if response.body.empty?

    code = response.body.sort_by! { |language| language['confidence'] }.first['language'].upcase

    Language.find(code)
  end

  def translate(content, from, to)
    response = Http.json.post("#{BASE_URI}/translate", q: content, source: from.downcase, target: to.downcase)

    return nil if response.status != 200

    response.body['translatedText']
  end

  def translate_all_languages(translation)
    Language.where('supported = true AND code != ?', translation.language_code).each do |language|
      new_translation = Translation.find_or_initialize_by(field: translation.field,
                                                          language_code: language.code,
                                                          resource_id: translation.resource_id,
                                                          resource_type: translation.resource_type)

      next if new_translation.edited?

      content = translate(translation.content, translation.language_code, language.code)

      next if content.nil?

      new_translation.content = content

      new_translation.save!
    end
  end
end
