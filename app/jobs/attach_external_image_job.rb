# frozen_string_literal: true

require 'open-uri'

class AttachExternalImageJob < ApplicationJob
  queue_as :default

  def perform(klass, id, image_type, image_url)
    image = klass.constantize.find(id).method(image_type).call

    file = URI.parse(image_url).open
    content_type = file.content_type

    image.attach(io: file,
                 filename: generate_filename(klass, id, image_type, content_type),
                 content_type: content_type)
  end

  def generate_filename(klass, id, image_type, content_type)
    extension = content_type.gsub(/[^a-z]/, ' ').split[1]

    "#{klass.downcase}-#{id}-#{image_type.to_s.dasherize}.#{extension}"
  end
end
