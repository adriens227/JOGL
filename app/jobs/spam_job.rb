# frozen_string_literal: true

# ..
class SpamJob < ApplicationJob
  queue_as :default

  def perform(klass, id, fields)
    instance = klass.constantize.find(id)

    hits = fields.filter_map { |field| check_field(instance, field) }

    return if hits.empty?

    SpamNotification.with(hits: hits, id: id, model: klass).deliver(User.with_role(:superadmin))
  end

  private

  def check_field(instance, field)
    text = instance.method(field).call

    language = instance.respond_to?(:language_code) ? instance.language_code : 'EN'
    file = "lib/spam/#{language}.crm"

    _, stdin, stdout, = Open4.popen4 "crm #{Rails.root.join(file)}"

    stdin.puts(text)
    stdin.close

    result = stdout.read

    result.presence ? field : nil
  end
end
