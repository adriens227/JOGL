# frozen_string_literal: true

class SearchJob < ApplicationJob
  queue_as :default

  BASE_URI = "http://#{ENV['MANTICORE_MASTER_SERVICE_HOST']}:#{ENV['MANTICORE_MASTER_SERVICE_PORT_HTTP']}".freeze

  def perform(method, *args)
    args.empty? ? method(method).call : method(method).call(*args)
  end

  private

  def build_doc(instance, search_fields, search_attributes)
    fields = instance.attributes.keep_if { |key| search_fields.include? key }

    attrs = search_attributes.each_with_object({}) do |chain, hash|
      methods = chain.split('.')

      value = methods.reduce(instance) do |i, m|
        next nil if i.nil?

        i.method(m).call
      end

      hash[methods.last(2).join('_')] = convert(value)
    end

    { 'resource_id' => instance.id, 'resource_type' => instance.class.name }.merge(fields).merge(attrs).compact
  end

  def convert(value)
    return value.to_time.to_i if value.respond_to?(:strftime)

    value
  end

  def deindex(klass, search_id)
    data = {
      'index' => klass.underscore.pluralize,
      'id' => search_id
    }

    Http.json.post("#{BASE_URI}/delete", wrap(data))
  end

  def index(klass, id, search_fields, search_attributes)
    instance = klass.constantize.find(id)

    return if instance.nil?

    data = {
      'index' => klass.underscore.pluralize,
      'id' => 0,
      'doc' => build_doc(instance, search_fields, search_attributes)
    }

    response = Http.json.post("#{BASE_URI}/insert", wrap(data))

    return if response.status != 200

    instance.update_column(:search_id, response.body['_id'])
  end

  def reindex(klass, id, search_fields, search_attributes)
    instance = klass.constantize.find(id)

    return if instance.nil?

    data = {
      'index' => klass.underscore.pluralize,
      'id' => instance.search_id,
      'doc' => build_doc(instance, search_fields, search_attributes)
    }

    Http.json.post("#{BASE_URI}/replace", wrap(data))
  end

  # If we are in a cluster environment we have to wrap the data with cluster information.
  def wrap(data)
    return data.merge({ 'cluster' => Rails.configuration.search_cluster_name }) unless Rails.configuration.search_cluster_name.nil?

    data
  end
end
