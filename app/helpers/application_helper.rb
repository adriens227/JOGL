# frozen_string_literal: true

module ApplicationHelper
  # Creates a title string for the current page (meta tag)
  #
  # @return [String]
  def title
    [content_for(:title), Rails.application.class.name.gsub(/::.*/, '').upcase].compact.join(' | ')
  end

  def enum_options(source)
    source.keys.map { |key| [key.humanize, key] }
  end

  # Show edit button if current_user can edit it
  #
  # @param type [ApplicationRecord] the type of object
  # @return [string]
  def show_edit_btn(object)
    link_to(t('form.edit'), edit_polymorphic_path(object), class: 'uk-button uk-button-primary') if can_edit(object)
  end

  # Check if current_user can edit the object (check if it's same user if object is User, else check if user is admin of that object)
  #
  # @param type [ApplicationRecord] the type of object
  # @return [Boolean] whether or not current_user can edit object
  def can_edit(object)
    object.instance_of?(User) ? (current_user&.id == object.id) : (current_user&.is_admin_of? object)
  end
end
