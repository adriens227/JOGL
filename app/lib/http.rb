# frozen_string_literal: true

require 'faraday_middleware'

# Wrapper around Faraday that provides some defaults.
module Http
  # Create a Faraday::Connection for sending/receiving JSON.
  #
  # @return [Farady::Connection] the connection
  def self.json
    Faraday.new do |conn|
      conn.request :json
      conn.request :retry

      conn.response :follow_redirects
      conn.response :json
    end
  end
end
