# frozen_string_literal: true

module Auth
  class ConfirmationsController < Devise::ConfirmationsController
    respond_to :json

    def show
      self.resource = resource_class.confirm_by_token(params[:confirmation_token])

      render status: :unprocessable_entity and return unless resource.errors.empty?

      redirect_to "#{Rails.configuration.frontend_url}/signin?confirmed=true"
    end
  end
end
