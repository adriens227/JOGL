# frozen_string_literal: true

class PermissionsController < ApplicationController
  before_action :authenticate_actor!
  before_action :load_type_and_class
  before_action :load_policy_with_type, unless: -> { params[:id].present? }
  before_action :load_policy_with_type_and_id, if: -> { params[:id].present? }
  before_action :construct_cache_key

  def index
    response = Rails.cache.fetch(@cache_key, expires_in: 5.minutes) do
      @policy.public_methods(false).concat(inherited_methods(@policy_klass)).uniq.filter_map do |action|
        action.to_s.camelize(:lower).delete_suffix('?') if @policy.apply(action)
      end.sort
    end

    render json: response
  end

  private

  def construct_cache_key
    base = [controller_name, action_name, @policy_klass, @record_klass]

    base.append(@record.id) if @record.present?

    @cache_key = base.append(current_actor.id).join(':')
  end

  def inherited_methods(klass)
    klass.superclass.public_instance_methods(false).filter { |method| method.to_s[-1] == '?' }
  end

  def load_type_and_class
    @policy_klass = ApplicationPolicy.descendants.find do |policy|
      policy.name == "#{params.require(:type).camelize}Policy"
    end

    @record_klass = @policy_klass.name.sub('Policy', '').constantize
  end

  def load_policy_with_type_and_id
    @record = @record_klass.find(params.require(:id))

    @policy = @policy_klass.new(@record, actor: current_actor)
  end

  def load_policy_with_type
    @policy = @policy_klass.new(actor: current_actor)
  end
end
