# frozen_string_literal: true

class DivisionsController < ApplicationController
  def index
    response = Rails.cache.fetch("#{controller_name}:#{action_name}", expires_in: 1.day) do
      DivisionBlueprint.render(Division.where(country_code: params.require(:country_code).upcase))
    end

    render json: response
  end
end
