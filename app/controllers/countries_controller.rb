# frozen_string_literal: true

class CountriesController < ApplicationController
  def index
    response = Rails.cache.fetch("#{controller_name}:#{action_name}", expires_in: 1.day) do
      CountryBlueprint.render(Country.all.order(code: :asc))
    end

    render json: response
  end

  def show
    country = Country.find(params.require(:code).upcase)

    render json: CountryBlueprint.render(country)
  end
end
