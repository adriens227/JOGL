# frozen_string_literal: true

class ApplicationController < ActionController::Base
  before_action :set_current_actor
  before_action :rate_limit
  before_action :set_locale
  respond_to :json
  skip_before_action :verify_authenticity_token

  CACHE = Redis.new(Rails.configuration.redis_options)

  attr_reader :current_actor

  def authenticate_actor!
    head :unauthorized if @current_actor.blank? || !(@current_actor.instance_of?(User) && @current_actor.confirmed?)
  end

  private

  def default_url_options
    { locale: I18n.locale }
  end

  def extract_locale
    parsed_locale = params[:locale]

    I18n.available_locales.map(&:to_s).include?(parsed_locale) ? parsed_locale : nil
  end

  def rate_limit
    key = @api_key.presence || request.ip
    requests_key = "requests:#{key}"
    blocked_key = "blocked:#{key}"

    head :too_many_requests if CACHE.get(blocked_key)

    cooldown, limit, window = Rails.configuration.rate_limit_options.values_at(:cooldown, :limit, :window)

    if CACHE.get(requests_key)
      requests = CACHE.incr(requests_key)

      if requests > limit
        Rails.logger.warn 'rate limit exceeded'
        # metric

        CACHE.set(blocked_key, 1)
        CACHE.expire(blocked_key, cooldown)

        head :too_many_requests
      end
    else
      CACHE.set(requests_key, 1)
      CACHE.expire(requests_key, window)
    end
  end

  def set_current_actor
    return if request.headers['X-API-KEY'].blank?

    key = Key.find(request.headers['X-API-KEY'])

    @api_key = key.id
    @current_actor = key.resource
  end

  def set_locale
    I18n.locale = extract_locale || I18n.default_locale
  end
end
