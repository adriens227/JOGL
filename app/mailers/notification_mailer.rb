# frozen_string_literal: true

# ..
class NotificationMailer < ApplicationMailer
  default from: "notifications@#{Rails.configuration.host}",
          message_id: "<#{Digest::SHA2.hexdigest(Time.now.to_i.to_s)}@#{Rails.configuration.host}>"

  layout 'notification_mailer'

  def notify
    @comment = @params[:comment]
    @notification = @params[:record].to_notification
    @recipient = @params[:recipient]

    mail(
      subject: @notification.subject,
      to: @recipient.email,
      template_path: 'notifications/mailer',
      template_name: 'notification'
    )
  end
end
