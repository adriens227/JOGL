# frozen_string_literal: true

class DivisionBlueprint < Blueprinter::Base
  identifier :code

  fields :name
end
