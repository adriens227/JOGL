# frozen_string_literal: true

class CountryBlueprint < Blueprinter::Base
  identifier :code

  fields :name
end
