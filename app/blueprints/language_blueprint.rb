# frozen_string_literal: true

class LanguageBlueprint < Blueprinter::Base
  identifier :code

  fields :name
end
