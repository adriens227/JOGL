# frozen_string_literal: true

class ApplicationRecord < ActiveRecord::Base
  primary_abstract_class

  after_initialize do |instance|
    instance.id = Digest::UUID.uuid_v4 if instance.id.nil? && instance.class.columns_hash['id']&.type == :uuid
  end
end
