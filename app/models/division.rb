# frozen_string_literal: true

# ISO 3166 recognized division of a Country.
#
# !@attribute [r] code
#   @return [String] ISO 3166 code
# !@attribute [r] country
#   @return [Country]
# !@attribute [r] name
#   @return [String] ISO 3166 name
class Division < ApplicationRecord
  include Searchable

  searchable %w[code name],
             %w[country_code]

  ## FIELDS ##
  self.primary_key = :code

  belongs_to :country, inverse_of: :divisions, foreign_key: :country_code

  counter_culture :country

  has_many :addresses, inverse_of: :division, foreign_key: :division_code, dependent: :destroy

  ## VALIDATIONS ##
  validates :code, :name, presence: true

  validates :code, length: { maximum: 7 }
  validates :name, length: { maximum: 128 }
end
