# frozen_string_literal: true

# ..
#
# @!attributes [rw] description
#   @return [String]
# @!attributes [r] space
#   @return [Space]
# @!attributes [rw] start
#   @return [Date]
# @!attributes [rw] stop
#   @return [Date]
# @!attributes [rw] summary
#   @return [String]
# @!attributes [rw] title
#   @return [String]
class Program < ApplicationRecord
  include Activityable
  include Assetable
  include Deletable
  include Graphable
  include Groupable
  include Feedable
  include Imageable
  include Mediable
  include Searchable
  include Sluggable
  include Spammable
  include SustainableDevelopmentGoalable
  include Taggable
  include TextSectionable
  include Translateable
  include Visibility

  graphable %w[start stop visibility]

  imageable banner: nil, logo: { thumb: { resize: '100x100' } }

  searchable %w[description summary title],
             %w[created_at
                space.id
                start
                stop
                updated_at
                visibility]

  sluggable field: :title

  spammable %w[description summary title]

  translateable %w[description summary title]

  ## FIELDS ##
  belongs_to :space, optional: true

  counter_culture :space

  has_many :roles, as: :resource, dependent: :destroy
  has_many :users, through: :roles, source: :user

  ## VALIDATIONS ##
  validates :description, presence: true
  validates :description, length: { maximum: 1024 }
  validates :summary, length: { maximum: 512 }
  validates :title, presence: true, length: { maximum: 128 }

  validates_with LogicalDateTimesValidator
end
