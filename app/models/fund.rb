# frozen_string_literal: true

# frozen_string_literal
class Fund < ApplicationRecord
  ## FIELDS ##
  belongs_to :resource, polymorphic: true

  ## VALIDATIONS ##
  validates :total, presence: true, numericality: { greater_than_or_equal_to: 0 }
end
