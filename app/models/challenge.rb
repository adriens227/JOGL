# frozen_string_literal: true

# A call to engage in contest or competition.
class Challenge < Activity
  include Activityable
  include Searchable
  include Spammable

  graphable %w[kind]

  imageable banner: nil, logo: nil

  searchable %w[description summary title],
             %w[created_at
                deadline
                location.address.country.code
                location.address.division.code
                location.latitude
                location.longitude
                start
                stop
                updated_at
                visibility]

  sluggable field: :title

  spammable %w[description summary title]

  translateable %w[description summary title]

  ## FIELDS ##
  has_many :accepted_challenges_projects, -> { accepted }, class_name: 'ChallengesProject', inverse_of: :challenge, dependent: :destroy
  has_many :projects, through: :accepted_challenges_projects, class_name: 'Project', source: :project, after_add: :accept!

  has_many :pending_challenges_projects, -> { pending }, class_name: 'ChallengesProject', inverse_of: :challenge, dependent: :destroy
  has_many :pending_projects, through: :pending_challenges_projects, class_name: 'Project', source: :project

  ## METHODS ##

  # Accept the given Project into the Challenge
  #
  # @param [Project]
  # @return [Project]
  def accept!(project)
    challenges_project = ChallengesProject.find_by(challenge_id: id, project_id: project.id)

    challenges_project.update!(accepted: true)
  end
end
