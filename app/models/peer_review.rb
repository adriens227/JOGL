# frozen_string_literal: true

class PeerReview < Activity
  include Activityable
  include Searchable
  include Spammable

  graphable %w[kind]

  imageable banner: nil

  optionable open_to_all: true

  searchable %w[description summary title],
             %w[created_at
                deadline
                location.address.country.code
                location.address.division.code
                location.latitude
                location.longitude
                start
                stop
                updated_at
                visibility]

  sluggable field: :title

  spammable %w[description summary title]

  translateable %w[description summary title]

  ## FIELDS ##
  has_one :fund, as: :resource, dependent: :destroy

  has_many :proposals, dependent: :destroy

  ## CALLBACKS ##
  after_initialize do |peer_review|
    peer_review.groups << Group.new(name: 'Reviewers')

    peer_review.text_sections << TextSection.new(category: :about, title: 'Rules')
  end
end
