# frozen_string_literal: true

# A physical place.
#
# @!attribute [rw] city_locality
#   @return [String] an incorporated center of population
# @!attribute [rw] country
#   @return [Country]
# @!attribute [rw] division
#   @return [Division]
# @!attribute [rw] postal_code
#   @return [String] an addition to a postal address to aid in sorting and delivery
# @!attribute [rw] street
#   @return [String] the number and road of a building on a thoroughfare, e.g., 123 Main Street
class Address < ApplicationRecord
  ## FIELDS ##
  belongs_to :country, foreign_key: :country_code, inverse_of: :addresses
  belongs_to :division, foreign_key: :division_code, inverse_of: :addresses

  ## VALIDATIONS ##
  validate :country_division_agreement

  ## METHODS ##

  # String representation of an Address.
  #
  # @return [String]
  def to_s
    [street, city_locality, division.name, country.name, postal_code].compact.join(', ')
  end

  private

  # The Division chosen is in the Country chosen.
  #
  # @return [nil]
  def country_division_agreement
    return if division.nil? || division.country == country

    errors.add(:division, 'division.country must match country')
  end
end
