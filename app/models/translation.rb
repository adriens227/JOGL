# frozen_string_literal: true

# A translated version of a piece of text.
#
# @!attribute [rw] content
#   @return [String]
# @!attribute [r] edited
#   @return [Boolean] whether or not the translation has been manually edited
# @!attribute [r] field
#   @return [String] the resource field that was translated
# @!attribute [r] language
#   @return [Language]
class Translation < ApplicationRecord
  include Searchable
  include Spammable

  searchable %w[content],
             %w[created_at language.code updated_at]

  spammable %w[content]

  ## FIELDS ##
  belongs_to :language, inverse_of: :translations, foreign_key: :language_code
  belongs_to :resource, polymorphic: true
end
