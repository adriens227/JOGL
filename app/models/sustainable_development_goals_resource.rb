# frozen_string_literal: true

# ..
class SustainableDevelopmentGoalsResource < ApplicationRecord
  ## FIELDS ##
  belongs_to :resource, polymorphic: true
  belongs_to :sustainable_development_goal

  ## VALIDATIONS ##
end
