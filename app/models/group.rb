# frozen_string_literal: true

# Named collection of Users belonging to a resource.
#
# !@attribute [rw] name
#   @return [String]
# !@attribute [rw] users
#   @return [Array<User>]
class Group < ApplicationRecord
  include Graphable

  ## FIELDS ##
  belongs_to :resource, polymorphic: true

  has_many :users_groups, dependent: nil
  has_many :users, through: :users_groups

  ## VALIDATIONS ##
  validates :name, presence: true
  validates :name, length: { maximum: 64 }
end
