# frozen_string_literal: true

# A happening in which Users can participate.
#
# @!attribute [rw] deadline
#   @return [DateTime]
# @!attribute [rw] description
#   @return [String]
# @!attribute [rw] kind
#   @return [String] whether this activity is a Challenge, Event, or Opportunity
# @!attribute [rw] location
#   @return [Location]
# @!attribute [rw] summary
#   @return [String]
# @!attribute [rw] start
#   @return [DateTime]
# @!attribute [rw] stop
#   #return [DateTime]
# @!attribute [rw] title
#   @return [String]
class Activity < ApplicationRecord
  include Activityable
  include Assetable
  include Fundable
  include Graphable
  include Groupable
  include Imageable
  include Mediable
  include Optionable
  include Skillable
  include Sluggable
  include SustainableDevelopmentGoalable
  include Taggable
  include TextSectionable
  include Translateable
  include Visibility

  ## FIELDS ##
  self.inheritance_column = :kind

  belongs_to :location, optional: true
  belongs_to :resource, polymorphic: true

  ## VALIDATIONS ##
  validates :kind, :title, presence: true
  validates :description, length: { maximum: 1024 }
  validates :summary, length: { maximum: 512 }
  validates :title, length: { maximum: 128 }

  validates_with LogicalDateTimesValidator

  ## SCOPES ##
  scope :challenges, -> { where(kind: 'Challenge') }
  scope :events, -> { where(kind: 'Event') }
  scope :opportunities, -> { where(kind: 'Opportunity') }
  scope :peer_reviews, -> { where(kind: 'PeerReview') }
end
