# frozen_string_literal: true

# ISO 3166 physical and legal boundary.
# !@attribute [r] code
#   @return [String] ISO 3166 code
# !@attribute [r] divisions_count
#   @return [Integer] number of Divisions in the Country
# !@attribute [r] name
#   @return [String] ISO 3166 name
class Country < ApplicationRecord
  include Graphable
  include Searchable

  searchable %w[code name]

  ## FIELDS ##
  self.primary_key = :code

  has_many :addresses, inverse_of: :country, foreign_key: :country_code, dependent: :destroy
  has_many :divisions, inverse_of: :country, foreign_key: :country_code, dependent: :destroy

  ## VALIDATIONS ##
  validates :code, :name, presence: true
  validates :code, length: { maximum: 2 }
  validates :name, length: { maximum: 64 }
end
