# frozen_string_literal: true

# Discrete time within a Project Phase.
#
# !@attribute [rw] date
#   @return [Date]
# !@attribute [rw] description
#   @return [String]
# !@attribute [r] phase
#   @return [Phase]
# !@attribute [rw] title
#   @return [String]
class Moment < ApplicationRecord
  include Translateable

  translateable %w[description title]

  ## FIELDS ##
  belongs_to :phase

  counter_culture %i[phase project]

  ## VALIDATIONS ##
  validates :date, :title, presence: true
  validates :description, length: { maximum: 1024 }
  validates :title, length: { maximum: 64 }
end
