# frozen_string_literal: true

# Textual response to a Post.
#
# !@attribute [rw] content
#   @return [String]
# !@attribute [rw] owner
#   @return [User]
# !@attribute [rw] post
#   @return [Post]
class Comment < ApplicationRecord
  include Graphable
  include Searchable
  include Spammable
  include Translateable

  searchable %w[content],
             %w[created_at
                owner.id
                post.id
                updated_at]

  spammable %w[content]

  translateable %w[content]

  ## FIELDS ##
  belongs_to :owner, class_name: 'User'
  belongs_to :post

  counter_culture :owner
  counter_culture :post

  ## VALIDATIONS ##
  validates :content, presence: true
  validates :content, length: { maximum: 1024 }

  ## CALLBACKS ##
  after_save -> { NewCommentNotification.with(comment: self).deliver_later(post.owner) }
end
