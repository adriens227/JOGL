# frozen_string_literal: true

# Distinct portion of time within a Project.
#
# !@attribute [rw] color
#   @return [String] hex representation of a color
# !@attribute [rw] start
#   @return [Date]
# !@attribute [rw] stop
#   @return [Date]
# !@attribute [rw] title
#   @return [String]
class Phase < ApplicationRecord
  include Graphable
  include Deletable
  include Translateable

  graphable %w[start stop]

  translateable %w[title]

  ## FIELDS ##
  belongs_to :project

  counter_culture :project

  has_many :moments, dependent: :destroy

  ## VALIDATIONS ##
  validates :start, :stop, :title, presence: true
  validates :title, length: { maximum: 64 }

  validates_with LogicalDateTimesValidator
end
