# frozen_string_literal: true

# A portion of text attached to a resource and constrained by kind.
#
# @!attribute [rw] category
#   @return [String] the category: about, faq, read more
# @!attribute [rw] content
#   @return [String] content
# @!attribute [rw] position
#   @return [Integer] relative position within a display
# @!attribute [rw] title
#   @return [String]
class TextSection < ApplicationRecord
  include Categorizable
  include Deletable
  include Searchable
  include Translateable

  categorizable about: 0, faq: 1, read_more: 2

  searchable %w[content title],
             %w[category created_at updated_at]

  translateable %w[content title]

  ## FIELDS ##
  belongs_to :resource, polymorphic: true

  ## VALIDATIONS ##
  validates :category, :title, presence: true
  validates :content, length: { maximum: 1024 }
  validates :title, length: { maximum: 64 }
end
