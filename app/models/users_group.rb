# frozen_string_literal: true

# Join Users into Groups.
class UsersGroup < ApplicationRecord
  ## FIELDS ##
  belongs_to :group
  belongs_to :user

  counter_culture :group, column_name: 'users_count'
  counter_culture :user, column_name: 'groups_count'

  ## VALIDATIONS ##
end
