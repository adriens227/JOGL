# frozen_string_literal: true

module Translateable
  extend ActiveSupport::Concern

  included do
    after_save :translate

    has_many :translations, as: :resource, dependent: :destroy

    def translate
      TranslationJob.perform_now(self.class.name, id, self.class.translated_fields)
    end

    def translated(language_code)
      language = Language.find(language_code)

      return nil if language.nil?

      translations = Translation.where(language_code: language.code, resource_id: id, resource_type: self.class.name)

      return if translations.empty?

      translations.each do |translation|
        method("#{translation.field}=").call(translation.content)
      end

      self
    end
  end

  class_methods do
    attr_reader :translated_fields

    private

    def translateable(translated_fields)
      @translated_fields = translated_fields
    end
  end
end
