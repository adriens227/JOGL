# frozen_string_literal: true

module Assetable
  extend ActiveSupport::Concern

  included do
    has_many :assets_resources, as: :resource, dependent: :destroy
    has_many :assets, through: :assets_resources
  end
end
