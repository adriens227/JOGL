# frozen_string_literal: true

module Imageable
  extend ActiveSupport::Concern

  included do
    before_save do
      self.class.image_options&.each_key do |type|
        image = method(type).call

        name = "#{self.class.name.underscore.dasherize}-#{image.name}"

        dir = Rails.root.join('app/assets/images')
        defaults = Dir.glob("#{dir}/default-#{name}-*.png")

        unless image.attached?
          image.attach(io: File.open(defaults.sample),
                       filename: "#{name}.png",
                       content_type: 'image/png')
        end
      end
    end
  end

  class_methods do
    attr_reader :image_options

    private

    def imageable(options = {})
      options.each do |type, variants|
        has_one_attached type do |attachable|
          variants&.each do |variant, config|
            next if config.present?

            attachable.variant variant, config
          end
        end

        validates type, content_size: 1_000_000
        validates type, content_type: %w[image/jpeg image/png]
      end

      @image_options = options
    end
  end
end
