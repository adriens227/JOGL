# frozen_string_literal: true

module Sluggable
  extend ActiveSupport::Concern

  SLUG_LENGTH = 64

  included do
    ## VALIDATIONS ##
    validates :slug,
              presence: true,
              uniqueness: true,
              length: { maximum: SLUG_LENGTH },
              format: { with: /\A[a-z0-9\-]*\z/, message: 'only alphanumeric and - characters allowed' }

    ## CALLBACKS ##
    before_validation -> { self.slug = generate_slug if slug.nil? }

    ## METHODS ##
    def generate_slug
      words = self.class.slug_field.nil? ? method(self.class.slug_method).call : self[self.class.slug_field]

      return nil if words.nil?

      words = words.gsub(/[^A-Za-z0-9\s]+/, '').split
      count = 0
      keep = []

      words.each do |word|
        break if (count + word.length + keep.length) >= SLUG_LENGTH

        keep.push(word)

        count += word.length
      end

      keep.join('-').downcase
    end

    def to_param
      slug
    end
  end

  class_methods do
    attr_reader :slug_field, :slug_method

    private

    def sluggable(options = {})
      @slug_field = options[:field]
      @slug_method = options[:method]
    end
  end
end
