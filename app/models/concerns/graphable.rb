# frozen_string_literal: true

module Graphable
  extend ActiveSupport::Concern

  DEFAULT_NODE_ATTRIBUTES = %w[created_at id updated_at].freeze

  included do
    after_create :merge_node

    before_update :delete_edges

    after_save :create_belongs_to_edges, :create_has_many_edges

    after_update :update_node

    after_destroy :delete_node

    def create_belongs_to_edges
      self.class.reflect_on_all_associations(:belongs_to).each do |thing|
        args = [self.class.name, id, thing.class_name, attributes["#{thing.name}_id"]]

        next if args[3].nil?

        GraphJob.perform_now('merge_edge', *args)
      end
    end

    def create_has_many_edges
      self.class.reflect_on_all_associations(:has_many).each do |thing|
        next unless thing.instance_of?(ActiveRecord::Reflection::ThroughReflection)

        method(thing.plural_name).call.each do |resource|
          args = [self.class.name, id, resource.class.name, resource.id]

          GraphJob.perform_now('merge_edge', *args)
        end
      end
    end

    def delete_edges
      GraphJob.perform_now('delete_edges', self.class.name, id)
    end

    def delete_node
      GraphJob.perform_now('delete_node', self.class.name, id)
    end

    def merge_node
      GraphJob.perform_now('merge_node', self.class.name, filter_attributes)
    end

    def update_node
      GraphJob.perform_now('update_node', self.class.name, filter_attributes)
    end

    private

    def filter_attributes
      node_attributes = self.class.node_attributes.presence || DEFAULT_NODE_ATTRIBUTES

      attributes.keep_if { |key| node_attributes.include? key }
    end
  end

  class_methods do
    attr_reader :node_attributes

    def graphable(attributes = [])
      @node_attributes = (attributes + DEFAULT_NODE_ATTRIBUTES).uniq
    end
  end
end
