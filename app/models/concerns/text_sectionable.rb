# frozen_string_literal: true

module TextSectionable
  extend ActiveSupport::Concern

  included do
    ## FIELDS ##
    has_many :text_sections, as: :resource, dependent: :destroy

    ## METHODS ##
    def grouped_text_sections
      TextSection.where(resource: self).order(:position).group_by(&:category)
    end
  end
end
