# frozen_string_literal: true

module Taggable
  extend ActiveSupport::Concern

  included do
    has_many :tags_resources, as: :resource, dependent: :destroy
    has_many :tags, through: :tags_resources
  end
end
