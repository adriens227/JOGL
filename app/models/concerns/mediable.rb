# frozen_string_literal: true

module Mediable
  extend ActiveSupport::Concern

  included do
    has_one :media, as: :resource, dependent: :destroy

    delegate :documents, :external_links, :images, to: :media

    after_create -> { self.media = Media.new(resource: self) }
  end
end
