# frozen_string_literal: true

module SustainableDevelopmentGoalable
  extend ActiveSupport::Concern

  included do
    has_many :sustainable_development_goals_resources, as: :resource, dependent: :destroy
    has_many :sustainable_development_goals, through: :sustainable_development_goals_resources
  end
end
