# frozen_string_literal: true

module Groupable
  extend ActiveSupport::Concern

  included do
    has_many :groups, as: :resource, dependent: :destroy
  end
end
