# frozen_string_literal: true

module Spammable
  extend ActiveSupport::Concern

  included do
    after_save :detect_spam

    def detect_spam
      SpamJob.perform_now(self.class.name, id, self.class.spam_fields)
    end
  end

  class_methods do
    attr_reader :spam_fields

    private

    def spammable(spam_fields)
      @spam_fields = spam_fields
    end
  end
end
