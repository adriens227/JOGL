# frozen_string_literal: true

# Pertinent textual information about a resource through its Feed.
#
# !@attribute [rw] category
#   @return [String] whether the Post is a discussion or announcement
# !@attribute [r] comments_count
#   @return [Integer] the number of comments on this Post
# !@attribute [rw] content
#   @return [String]
# !@attribute [r] feed
#   @return [Feed]
# !@attribute [r] owner
#   @return [User]
class Post < ApplicationRecord
  include Categorizable
  include Graphable
  include Searchable
  include Spammable
  include Translateable

  categorizable discussion: 0, announcement: 1

  graphable %w[category]

  searchable %w[content],
             %w[category
                created_at
                feed.resource_id
                feed.resource_type
                owner.id
                updated_at]

  spammable %w[content]

  translateable %w[content]

  ## FIELDS ##
  belongs_to :feed
  belongs_to :owner, class_name: 'User'

  counter_culture :feed
  counter_culture :owner

  has_many :comments, dependent: :destroy
  has_many :mentions, dependent: :destroy

  ## VALIDATIONS ##
  validates :category, :content, presence: true
  validates :content, length: { maximum: 1024 }
end
