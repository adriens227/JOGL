# frozen_string_literal: true

class Key < ApplicationRecord
  belongs_to :resource, polymorphic: true
end
