# frozen_string_literal: true

# Ability, proficiency, or talent.
#
# @!attribute [rw] name
#   @return [String]
class Skill < ApplicationRecord
  include Graphable
  include Searchable
  include Sluggable
  include Translateable

  searchable %w[name],
             %w[created_at updated_at]

  sluggable field: :name

  translateable %w[name]

  ## FIELDS ##
  has_many :skills_resources, dependent: :destroy
  has_many :activities, through: :skills_resources, source: :resource, source_type: 'Activity'
  has_many :projects, through: :skills_resources, source: :resource, source_type: 'Project'
  has_many :spaces, through: :skills_resources, source: :resource, source_type: 'Space'
  has_many :users, through: :skills_resources, source: :resource, source_type: 'User'

  ## VALIDATIONS ##
  validates :name, presence: true, length: { maximum: 64 }
end
