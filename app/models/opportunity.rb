# frozen_string_literal: true

# A chance to participate by providing labor, Skills, or Assets.
class Opportunity < Activity
  include Activityable
  include Searchable
  include Spammable

  graphable %w[kind]

  imageable logo: nil

  searchable %w[description title],
             %w[created_at
                deadline
                kind
                location.address.country.code
                location.address.division.code
                location.latitude
                location.longitude
                start
                stop
                updated_at
                visibility]

  sluggable field: :title

  spammable %w[description title]

  translateable %w[description title]
end
