# frozen_string_literal: true

# ..
#
# @!attributes [rw] category
#   @return [String] the type of Space
# @!attributes [rw] description
#   @return [String]
# @!attributes [rw] options
#   @return [Hash]
# @!attributes [r] owner
#   @return [User]
# @!attributes [r] programs_count
#   @return [Integer] the number of Programs in the Space
# @!attributes [r] projects_count
#   @return [Integer] the number of Projects in the Space
# @!attributes [r] space
#   @return [Space]
# @!attributes [r] spaces_count
#   @return [Integer] the number of sub spaces in the Space
# @!attributes [rw] start
#   @return [Date]
# @!attributes [rw] stop
#   @return [Date]
# @!attributes [rw] summary
#   @return [String]
# @!attributes [rw] title
#   @return [String]
class Space < ApplicationRecord
  include Activityable
  include Assetable
  include Categorizable
  include Deletable
  include Graphable
  include Groupable
  include Feedable
  include Imageable
  include Mediable
  include Optionable
  include Searchable
  include Sluggable
  include Spammable
  include SustainableDevelopmentGoalable
  include Taggable
  include TextSectionable
  include Translateable
  include Visibility

  categorizable(
    other: 0,
    local_community: 1,
    ngo: 2,
    not_for_profit_organization: 3,
    startup: 4,
    maker_space: 5,
    community_lab: 6,
    company: 7,
    social_enterprise: 8,
    school: 9,
    foundation: 10,
    academic_lab: 11,
    professional_network: 12,
    public_agency: 13,
    public_institution: 14,
    incubator: 15,
    living_lab: 16,
    fund: 17,
    digital_community: 18,
    workgroup: 19
  )

  feedable allow_posting_to_all: false

  graphable %w[category visibility]

  imageable banner: nil, logo: { thumb: { resize: '100x100' } }

  optionable(
    show_associated_projects: false,
    show_associated_users: false,
    show_featured_activities: false,
    show_featured_programs: false,
    show_featured_projects: false
  )

  searchable %w[description summary title],
             %w[category
                created_at
                owner.id
                updated_at
                visibility]

  sluggable field: :title

  spammable %w[description title]

  translateable %w[description summary title]

  ## FIELDS ##
  belongs_to :owner, class_name: 'User'
  belongs_to :space, optional: true

  counter_culture :owner
  counter_culture :space

  has_many :projects, dependent: :destroy
  has_many :roles, as: :resource, dependent: :destroy
  has_many :spaces, dependent: :destroy
  has_many :users, through: :roles, source: :user

  ## VALIDATIONS ##
  validates :description, :title, presence: true
  validates :description, length: { maximum: 1024 }
  validates :summary, length: { maximum: 512 }
  validates :title, length: { maximum: 128 }
end
