# frozen_string_literal: true

class ChallengesProject < ApplicationRecord
  ## FIELDS ##
  belongs_to :challenge
  belongs_to :project

  ## VALIDATIONS ##

  ## SCOPES ##
  scope :accepted, -> { where(accepted: true) }
  scope :pending, -> { where(accepted: false) }
end
