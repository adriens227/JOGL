# frozen_string_literal: true

# Any thing possessed by an individual, group, or other entity.
#
# @!attribute [rw] name
#   @return [String]
class Asset < ApplicationRecord
  include Graphable
  include Searchable
  include Translateable

  searchable %w[name],
             %w[created_at updated_at]

  translateable %w[name]

  ## FIELDS ##
  has_many :assets_resources, dependent: :destroy
  has_many :activities, through: :assets_resources, source: :resource, source_type: 'Activity'
  has_many :users, through: :assets_resources, source: :resource, source_type: 'User'
  has_many :spaces, through: :assets_resources, source: :resource, source_type: 'Space'

  ## VALIDATIONS ##
  validates :name, presence: true, length: { maximum: 64 }
end
