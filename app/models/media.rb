# frozen_string_literal: true

# Collection of documents, images, and ExternalLinks attached to a resource.
#
# !@attribute [rw] documents
#   @return [Array<ActiveStorage::Attachment>]
# !@attribute [rw] external_links
#   @return [Array<ExternalLink>]
# !@attribute [rw] images
#   @return [Array<ActiveStorage::Attachment>]
class Media < ApplicationRecord
  include Deletable

  ## FIELDS ##
  belongs_to :resource, polymorphic: true

  has_many :external_links, as: :resource, dependent: :destroy

  has_many_attached :documents
  has_many_attached :images

  ## VALIDATIONS ##
  validates :documents, content_size: 10_000_000
  validates :documents, content_type: %w[application/msword
                                         application/pdf
                                         application/vnd.ms-excel
                                         application/vnd.ms-powerpoint
                                         application/vnd.openxmlformats-officedocument.presentationml.presentation
                                         application/vnd.openxmlformats-officedocument.spreadsheetml.sheet
                                         application/vnd.openxmlformats-officedocument.wordprocessingml.document
                                         text/plain]

  validates :images, content_size: 1_000_000
  validates :images, content_type: %w[image/gif image/jpeg image/png image/svg+xml]
end
