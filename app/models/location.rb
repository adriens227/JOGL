# frozen_string_literal: true

# Physical place.
#
# !@attribute [rw] address
#   @return [Address]
# !@attribute [r] latitude
#   @return [Float] North/South position
# !@attribute [r] longitude
#   @return [Float] East/West position
# !@attribute [rw] name
#   @return [String]
class Location < ApplicationRecord
  include Graphable
  include Searchable

  geocoded_by :geocode_by

  graphable %w[name]

  searchable %w[name],
             %w[address.country.code
                address.division.code
                latitude
                longitude]

  ## FIELDS ##
  belongs_to :address, optional: true

  ## VALIDATIONS ##
  validates :name, length: { maximum: 64 }

  ## CALLBACKS ##
  after_validation :geocode

  ## METHODS ##

  # :nocov:

  # @return [Array<Float>] the latitude and longitude
  def coordinates
    return nil if latitude.nil? || longitude.nil?

    [latitude, longitude]
  end

  # @return [Array<Float>] the latitude and longitude in radians
  def coordinates_in_radians
    return nil if latitude.nil? || longitude.nil?

    [latitude_in_radians, longitude_in_radians]
  end

  # @return [Float] the latitude in radians
  def latitude_in_radians
    return nil if latitude.nil?

    Math::PI * latitude / 180.0
  end

  # @return [Float] the longitude in radians
  def longitude_in_radians
    return nil if longitude.nil?

    Math::PI * longitude / 180.0
  end

  # :nocov:

  private

  def geocode_by
    address.to_s
  end
end
