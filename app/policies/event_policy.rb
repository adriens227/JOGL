# frozen_string_literal: true

class EventPolicy < ActionPolicy::Base
  def destroy?
    allowed_to? :destroy?, record.resource
  end

  def update?
    allowed_to? :update?, record.resource
  end
end
