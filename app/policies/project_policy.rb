# frozen_string_literal: true

class ProjectPolicy < ApplicationPolicy
  def create_challenge?
    admin?
  end

  def create_event?
    admin?
  end

  def create_moment?
    admin?
  end

  def create_opportunity?
    admin?
  end

  def create_phase?
    admin?
  end
end
