# frozen_string_literal: true

class SpacePolicy < ApplicationPolicy
  def create_event?
    admin?
  end

  def create_opportunity?
    admin?
  end
end
