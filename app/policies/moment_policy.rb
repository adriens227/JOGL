# frozen_string_literal: true

class MomentPolicy < ApplicationPolicy
  def destroy?
    allowed_to? :destroy?, record.phase.project
  end

  def update?
    allowed_to? :update?, record.phase.project
  end
end
