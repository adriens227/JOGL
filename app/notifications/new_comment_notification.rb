# frozen_string_literal: true

# ..
class NewCommentNotification < ApplicationNotification
  deliver_by :database
  deliver_by :email, mailer: 'NotificationMailer', method: 'notify', if: :email_notifications?, unless: :abandon?

  param :comment

  def abandon?
    read? || params[:comment].owner.id == params[:comment].post.owner.id
  end

  def message
    params[:comment].content
  end

  def subject
    t('.subject', user: params[:comment].owner.full_name)
  end

  def url
    # url_for(params[:comment].post.feed.resource)
    '/'
  end
end
