# frozen_string_literal: true

class SpamNotification < ApplicationNotification
  deliver_by :database

  param :hits, :id, :model
end
