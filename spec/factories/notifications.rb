# frozen_string_literal: true

# == Schema Information
#
# Table name: notifications
#
#  id             :uuid             not null, primary key
#  recipient_id   :uuid             not null
#  recipient_type :string           not null
#  params         :jsonb
#  type           :string           not null
#  read_at        :datetime
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
FactoryBot.define do
  factory :notification do
    recipient { build(:user) }
    type { FFaker::Color.name.titlecase }
  end
end
