# frozen_string_literal: true

# == Schema Information
#
# Table name: funds
#
#  id                   :uuid             not null, primary key
#  resource_id          :uuid             not null
#  resource_type        :string           not null
#  total                :float            default(0.0), not null
#  maximum_disbursement :float
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
FactoryBot.define do
  factory :fund do
    resource { create(:peer_review) }
    total { 10_000.00 }
  end
end
