# frozen_string_literal: true

# == Schema Information
#
# Table name: sustainable_development_goals
#
#  id        :uuid             not null, primary key
#  color     :string(6)        default("000000"), not null
#  name      :string(64)       not null
#  number    :integer          not null
#  search_id :bigint
#
FactoryBot.define do
  factory :sustainable_development_goal do
    color { FFaker::Color.hex_code.upcase }
    name { FFaker::Book.title[0..63] }
    number { 1 }
  end
end
