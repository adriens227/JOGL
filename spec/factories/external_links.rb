# frozen_string_literal: true

# == Schema Information
#
# Table name: external_links
#
#  id            :uuid             not null, primary key
#  resource_id   :uuid             not null
#  resource_type :string           not null
#  text          :string(128)      not null
#  url           :string(128)      not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
FactoryBot.define do
  factory :external_link do
    text { FFaker::Book.title[0..63] }
    resource { build(:space) }
    url { FFaker::Internet.http_url }
  end
end
