# frozen_string_literal: true

# == Schema Information
#
# Table name: spaces
#
#  id             :uuid             not null, primary key
#  owner_id       :uuid             not null
#  space_id       :uuid
#  category       :integer          default("other"), not null
#  deleted        :boolean          default(FALSE), not null
#  description    :string(1024)
#  options        :jsonb            not null
#  search_id      :bigint
#  slug           :string(64)       not null
#  summary        :string(512)
#  title          :string(128)      not null
#  visibility     :integer          default("hidden"), not null
#  programs_count :integer          default(0), not null
#  projects_count :integer          default(0), not null
#  spaces_count   :integer          default(0), not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
FactoryBot.define do
  factory :space do
    description { FFaker::Book.description[0..511] }
    owner { build(:user) }
    title { FFaker::Book.title[0..127] }
  end
end
