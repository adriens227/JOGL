# frozen_string_literal: true

# == Schema Information
#
# Table name: comments
#
#  id         :uuid             not null, primary key
#  owner_id   :uuid             not null
#  post_id    :uuid             not null
#  content    :string(1024)     not null
#  deleted    :boolean          default(FALSE), not null
#  search_id  :bigint
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :comment do
    content { FFaker::Book.description[0..255] }
    owner { create(:user) }
    post { create(:post) }
  end
end
