# frozen_string_literal: true

# == Schema Information
#
# Table name: skills
#
#  id         :uuid             not null, primary key
#  name       :string(64)       not null
#  search_id  :bigint
#  slug       :string(64)       not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
FactoryBot.define do
  factory :skill do
    name { FFaker::Book.title[0..63] }
  end
end
