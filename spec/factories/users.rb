# frozen_string_literal: true

FactoryBot.define do
  factory :user do
    description { FFaker::DizzleIpsum.paragraph[0..127] }
    email { FFaker::Internet.email }
    first_name { FFaker::Name.first_name }
    last_name { FFaker::Name.last_name }
    location { build(:location) }
    password { 'password' }
    password_confirmation { 'password' }
  end
end
