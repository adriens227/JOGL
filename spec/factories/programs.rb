# frozen_string_literal: true

# == Schema Information
#
# Table name: programs
#
#  id          :uuid             not null, primary key
#  space_id    :uuid
#  deleted     :boolean          default(FALSE), not null
#  description :string
#  search_id   :bigint
#  slug        :string           not null
#  summary     :string
#  start       :date
#  stop        :date
#  title       :string           not null
#  visibility  :integer          default("hidden"), not null
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#
FactoryBot.define do
  factory :program do
    description { FFaker::Book.description[0..511] }
    start { Time.zone.now }
    stop { 1.week.from_now }
    title { FFaker::Book.title[0..127] }
  end
end
