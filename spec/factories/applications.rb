# frozen_string_literal: true

FactoryBot.define do
  factory :application do
    peer_review { build(:peer_review) }
    project { build(:project) }
  end
end
