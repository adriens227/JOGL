# frozen_string_literal: true

FactoryBot.define do
  factory :role do
    name { :follower }
    resource { build(:space) }
    user { build(:user) }
  end
end
