# frozen_string_literal: true

# == Schema Information
#
# Table name: groups
#
#  id            :uuid             not null, primary key
#  resource_id   :uuid             not null
#  resource_type :string           not null
#  name          :string(64)
#  users_count   :integer          default(0), not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
FactoryBot.define do
  factory :group do
    name { FFaker::Book.title[0..63] }
    resource { build(:space) }
  end
end
