# frozen_string_literal: true

# == Schema Information
#
# Table name: mentions
#
#  id            :uuid             not null, primary key
#  post_id       :uuid             not null
#  resource_id   :uuid             not null
#  resource_type :string           not null
#  deleted       :boolean          default(FALSE), not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
FactoryBot.define do
  factory :mention do
    post { create(:post) }
    resource { build(:space) }
  end
end
