# frozen_string_literal: true

# == Schema Information
#
# Table name: posts
#
#  id             :uuid             not null, primary key
#  feed_id        :uuid             not null
#  owner_id       :uuid             not null
#  category       :integer          default("discussion"), not null
#  content        :string(1024)     not null
#  deleted        :boolean          default(FALSE), not null
#  search_id      :bigint
#  comments_count :integer          default(0), not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
FactoryBot.define do
  factory :post do
    category { Post.categories.keys[rand(Post.categories.keys.count - 1)] }
    content { FFaker::Book.description[0..255] }
    feed { create(:space).feed }
    owner { build(:user) }
  end
end
