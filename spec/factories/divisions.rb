# frozen_string_literal: true

# == Schema Information
#
# Table name: divisions
#
#  code         :string(7)        not null, primary key
#  country_code :string(2)        not null
#  name         :string(128)      not null
#  search_id    :bigint
#
FactoryBot.define do
  factory :division do
    code { 'FR-75' }
    country_code { 'FR' }
    name { 'Paris' }
  end
end
