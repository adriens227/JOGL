# frozen_string_literal: true

require 'simplecov'
require 'webmock/rspec'

RSpec.configure do |config|
  config.before(:suite) do
    DatabaseCleaner.clean_with(:truncation)
  end

  config.before do
    stub_request(:any, /www.eventbriteapi.com/).to_rack(FakeEventbrite)
    stub_request(:any, /:5000/).to_rack(FakeLibreTranslate)
  end

  config.expect_with :rspec do |expectations|
    expectations.include_chain_clauses_in_custom_matcher_descriptions = true
  end

  config.mock_with :rspec do |mocks|
    mocks.verify_partial_doubles = true
  end

  config.shared_context_metadata_behavior = :apply_to_host_groups
end

SimpleCov.start do
  add_filter %w[/config/ /db/ /policies/ /spec/ /vendor/]
end

WebMock.allow_net_connect!
