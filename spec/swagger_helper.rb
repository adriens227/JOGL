# frozen_string_literal: true

require 'rails_helper'

RSpec.configure do |config|
  config.swagger_root = Rails.root.join('swagger').to_s

  config.swagger_docs = {
    'v1/swagger.json' => {
      openapi: '3.0.1',
      info: {
        title: 'JOGL',
        version: 'v1'
      },
      paths: {},
      servers: [
        {
          url: "#{Rails.env.production? ? 'https' : 'http'}://{defaultHost}",
          variables: {
            defaultHost: {
              default: Rails.configuration.host
            }
          }
        }
      ],
      components: {
        securitySchemes: {
          ApiKeyAuth: {
            type: :apiKey,
            in: :header,
            name: 'X-API-KEY'
          }
        },
        schemas: {
          country: {
            type: :object,
            properties: {
              code: { type: :string },
              name: { type: :string }
            }
          },
          division: {
            type: :object,
            properties: {
              code: { type: :string },
              name: { type: :string }
            }
          },
          language: {
            type: :object,
            properties: {
              code: { type: :string },
              name: { type: :string }
            }
          },
          role: {
            type: :object,
            properties: {
              name: { type: :string },
              resource_id: { type: :string },
              resource_type: { type: :string }
            }
          }
        }
      }
    }
  }

  config.swagger_format = :json
end
