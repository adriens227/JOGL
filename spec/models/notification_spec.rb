# frozen_string_literal: true

# == Schema Information
#
# Table name: notifications
#
#  id             :uuid             not null, primary key
#  recipient_id   :uuid             not null
#  recipient_type :string           not null
#  params         :jsonb
#  type           :string           not null
#  read_at        :datetime
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
require 'rails_helper'

RSpec.describe Notification, type: :model do
  subject { create(:notification) }

  it { is_expected.to belong_to(:recipient) }

  it { is_expected.to validate_presence_of(:type) }
end
