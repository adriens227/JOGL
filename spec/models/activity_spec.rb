# frozen_string_literal: true

# == Schema Information
#
# Table name: activities
#
#  id            :uuid             not null, primary key
#  activity_id   :uuid
#  location_id   :uuid
#  resource_id   :uuid             not null
#  resource_type :string           not null
#  deadline      :datetime
#  description   :string(1024)
#  kind          :string(32)       not null
#  options       :jsonb            not null
#  search_id     :bigint
#  slug          :string(64)       not null
#  start         :datetime
#  stop          :datetime
#  summary       :string(512)
#  title         :string(128)      not null
#  visibility    :integer          default("hidden"), not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
require 'rails_helper'

require Rails.root.join('spec/models/concerns/activityable.rb')
require Rails.root.join('spec/models/concerns/fundable.rb')
require Rails.root.join('spec/models/concerns/groupable.rb')
require Rails.root.join('spec/models/concerns/mediable.rb')
require Rails.root.join('spec/models/concerns/sustainable_development_goalable.rb')
require Rails.root.join('spec/models/concerns/taggable.rb')
require Rails.root.join('spec/models/concerns/visibility.rb')

RSpec.describe Activity, type: :model do
  subject { build(:event) }

  it_behaves_like 'activityable'
  it_behaves_like 'fundable'
  it_behaves_like 'groupable'
  it_behaves_like 'mediable'
  it_behaves_like 'sustainable_development_goalable'
  it_behaves_like 'taggable'
  it_behaves_like 'visibility'

  it { is_expected.to belong_to(:location).optional }

  it { is_expected.to validate_presence_of(:kind) }
  it { is_expected.to validate_presence_of(:title) }
end
