# frozen_string_literal: true

require 'rails_helper'

require Rails.root.join('spec/models/concerns/assetable.rb')
require Rails.root.join('spec/models/concerns/deletable.rb')
require Rails.root.join('spec/models/concerns/feedable.rb')
require Rails.root.join('spec/models/concerns/mediable.rb')
require Rails.root.join('spec/models/concerns/optionable.rb')
require Rails.root.join('spec/models/concerns/searchable.rb')
require Rails.root.join('spec/models/concerns/skillable.rb')
require Rails.root.join('spec/models/concerns/sluggable.rb')
require Rails.root.join('spec/models/concerns/spammable.rb')
require Rails.root.join('spec/models/concerns/sustainable_development_goalable.rb')
require Rails.root.join('spec/models/concerns/taggable.rb')
require Rails.root.join('spec/models/concerns/translateable.rb')

RSpec.describe User, type: :model do
  subject { create(:user) }

  it_behaves_like 'assetable'
  it_behaves_like 'deletable'
  it_behaves_like 'feedable'
  it_behaves_like 'mediable'
  it_behaves_like 'optionable'
  it_behaves_like 'searchable'
  it_behaves_like 'skillable'
  it_behaves_like 'sluggable'
  it_behaves_like 'spammable'
  it_behaves_like 'sustainable_development_goalable'
  it_behaves_like 'taggable'
  it_behaves_like 'translateable'

  it { is_expected.to belong_to(:location).optional }

  it { is_expected.to have_many(:notifications) }
  it { is_expected.to have_many(:posts) }
  it { is_expected.to have_many(:projects) }
  it { is_expected.to have_many(:spaces) }
  it { is_expected.to have_many(:users_groups) }

  it { is_expected.to validate_presence_of(:first_name) }
  it { is_expected.to validate_presence_of(:last_name) }
  it { is_expected.to validate_length_of(:first_name) }
  it { is_expected.to validate_length_of(:last_name) }
  it { is_expected.to validate_length_of(:username) }

  it 'creates a full name' do
    expect(subject.full_name).not_to be_empty
  end

  it 'generates a username' do
    expect(subject.username).not_to be_empty
  end

  it 'grants a role on no resource' do
    user = create(:user)
    user.grant_role(:superadmin)

    expect(user.role?(:superadmin)).to be true
  end

  it 'grants a role on a resource' do
    user = create(:user)
    space = create(:space)
    user.grant_role(:admin, space)

    expect(user.role?(:admin, space)).to be true
  end

  it 'only grants one role at a time' do
    user = create(:user)
    space = create(:space)

    user.grant_role(:moderator, space)
    user.grant_role(:admin, space)

    expect(user.roles.map(&:name)).not_to include(:moderator)
  end

  it 'revokes a role' do
    user = create(:user)
    space = create(:space)
    create(:role, name: :member, resource: space, user: user)

    user.revoke_role(:member, space)

    expect(user.roles).to be_empty
  end
end
