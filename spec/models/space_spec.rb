# frozen_string_literal: true

# == Schema Information
#
# Table name: spaces
#
#  id             :uuid             not null, primary key
#  owner_id       :uuid             not null
#  space_id       :uuid
#  category       :integer          default("other"), not null
#  deleted        :boolean          default(FALSE), not null
#  description    :string(1024)
#  options        :jsonb            not null
#  search_id      :bigint
#  slug           :string(64)       not null
#  summary        :string(512)
#  title          :string(128)      not null
#  visibility     :integer          default("hidden"), not null
#  programs_count :integer          default(0), not null
#  projects_count :integer          default(0), not null
#  spaces_count   :integer          default(0), not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
require 'rails_helper'

require Rails.root.join('spec/models/concerns/activityable.rb')
require Rails.root.join('spec/models/concerns/assetable.rb')
require Rails.root.join('spec/models/concerns/categorizable.rb')
require Rails.root.join('spec/models/concerns/deletable.rb')
require Rails.root.join('spec/models/concerns/feedable.rb')
require Rails.root.join('spec/models/concerns/groupable.rb')
require Rails.root.join('spec/models/concerns/mediable.rb')
require Rails.root.join('spec/models/concerns/optionable.rb')
require Rails.root.join('spec/models/concerns/searchable.rb')
require Rails.root.join('spec/models/concerns/sluggable.rb')
require Rails.root.join('spec/models/concerns/spammable.rb')
require Rails.root.join('spec/models/concerns/taggable.rb')
require Rails.root.join('spec/models/concerns/text_sectionable.rb')
require Rails.root.join('spec/models/concerns/translateable.rb')
require Rails.root.join('spec/models/concerns/visibility.rb')

RSpec.describe Space, type: :model do
  subject { create(:space) }

  it_behaves_like 'activityable'
  it_behaves_like 'assetable'
  it_behaves_like 'categorizable'
  it_behaves_like 'deletable'
  it_behaves_like 'feedable'
  it_behaves_like 'groupable'
  it_behaves_like 'mediable'
  it_behaves_like 'optionable'
  it_behaves_like 'searchable'
  it_behaves_like 'sluggable'
  it_behaves_like 'spammable'
  it_behaves_like 'taggable'
  it_behaves_like 'text_sectionable'
  it_behaves_like 'translateable'
  it_behaves_like 'visibility'

  it { is_expected.to belong_to(:owner) }
  it { is_expected.to belong_to(:space).optional }

  it { is_expected.to have_many(:projects) }
  it { is_expected.to have_many(:spaces) }

  it { is_expected.to validate_presence_of(:description) }
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_length_of(:description) }
  it { is_expected.to validate_length_of(:summary) }
  it { is_expected.to validate_length_of(:title) }
end
