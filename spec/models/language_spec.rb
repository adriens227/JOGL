# frozen_string_literal: true

# == Schema Information
#
# Table name: languages
#
#  code      :string(2)        not null, primary key
#  name      :string(96)       not null
#  search_id :bigint
#  supported :boolean          default(FALSE), not null
#
require 'rails_helper'

RSpec.describe Language, type: :model do
  subject { described_class.first }

  it { is_expected.to validate_presence_of(:code) }
  it { is_expected.to validate_presence_of(:name) }

  it { is_expected.to validate_length_of(:code) }
  it { is_expected.to validate_length_of(:name) }
end
