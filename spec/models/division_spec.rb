# frozen_string_literal: true

# == Schema Information
#
# Table name: divisions
#
#  code         :string(7)        not null, primary key
#  country_code :string(2)        not null
#  name         :string(128)      not null
#  search_id    :bigint
#
require 'rails_helper'

RSpec.describe Division, type: :model do
  subject { described_class.first }

  it { is_expected.to belong_to(:country) }

  it { is_expected.to validate_presence_of(:code) }
  it { is_expected.to validate_presence_of(:name) }

  it { is_expected.to validate_length_of(:code) }
  it { is_expected.to validate_length_of(:name) }
end
