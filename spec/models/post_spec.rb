# frozen_string_literal: true

# == Schema Information
#
# Table name: posts
#
#  id             :uuid             not null, primary key
#  feed_id        :uuid             not null
#  owner_id       :uuid             not null
#  category       :integer          default("discussion"), not null
#  content        :string(1024)     not null
#  deleted        :boolean          default(FALSE), not null
#  search_id      :bigint
#  comments_count :integer          default(0), not null
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
require 'rails_helper'

require Rails.root.join('spec/models/concerns/categorizable.rb')
require Rails.root.join('spec/models/concerns/searchable.rb')
require Rails.root.join('spec/models/concerns/spammable.rb')
require Rails.root.join('spec/models/concerns/translateable.rb')

RSpec.describe Post, type: :model do
  subject { create(:post) }

  it_behaves_like 'categorizable'
  it_behaves_like 'searchable'
  it_behaves_like 'spammable'
  it_behaves_like 'translateable'

  it { is_expected.to belong_to(:feed) }
  it { is_expected.to belong_to(:owner) }

  it { is_expected.to validate_presence_of(:content) }
  it { is_expected.to validate_length_of(:content) }
end
