# frozen_string_literal: true

# == Schema Information
#
# Table name: projects
#
#  id            :uuid             not null, primary key
#  location_id   :uuid
#  owner_id      :uuid             not null
#  space_id      :uuid
#  deleted       :boolean          default(FALSE), not null
#  description   :string(1024)     not null
#  search_id     :bigint
#  slug          :string(64)       not null
#  summary       :string(512)
#  title         :string(128)      not null
#  visibility    :integer          default("hidden"), not null
#  moments_count :integer          default(0), not null
#  phases_count  :integer          default(0), not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
require 'rails_helper'

require Rails.root.join('spec/models/concerns/deletable.rb')
require Rails.root.join('spec/models/concerns/feedable.rb')
require Rails.root.join('spec/models/concerns/mediable.rb')
require Rails.root.join('spec/models/concerns/searchable.rb')
require Rails.root.join('spec/models/concerns/sluggable.rb')
require Rails.root.join('spec/models/concerns/spammable.rb')
require Rails.root.join('spec/models/concerns/sustainable_development_goalable.rb')
require Rails.root.join('spec/models/concerns/taggable.rb')
require Rails.root.join('spec/models/concerns/text_sectionable.rb')
require Rails.root.join('spec/models/concerns/translateable.rb')
require Rails.root.join('spec/models/concerns/visibility.rb')

RSpec.describe Project, type: :model do
  subject { create(:project) }

  it_behaves_like 'deletable'
  it_behaves_like 'feedable'
  it_behaves_like 'mediable'
  it_behaves_like 'searchable'
  it_behaves_like 'sluggable'
  it_behaves_like 'spammable'
  it_behaves_like 'sustainable_development_goalable'
  it_behaves_like 'taggable'
  it_behaves_like 'text_sectionable'
  it_behaves_like 'translateable'
  it_behaves_like 'visibility'

  it { is_expected.to belong_to(:location).optional }
  it { is_expected.to belong_to(:owner) }
  it { is_expected.to belong_to(:space).optional }

  it { is_expected.to have_many(:phases) }

  it { is_expected.to validate_presence_of(:description) }
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_length_of(:description) }
  it { is_expected.to validate_length_of(:summary) }
  it { is_expected.to validate_length_of(:title) }

  it 'can join a challenge as accepted' do
    challenge = create(:challenge)

    challenge.projects << subject

    expect(challenge.projects).not_to be_empty
  end

  it 'can join a challenge as pending' do
    challenge = create(:challenge)

    challenge.pending_projects << subject

    expect(challenge.pending_projects).not_to be_empty
  end
end
