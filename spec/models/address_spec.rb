# frozen_string_literal: true

# == Schema Information
#
# Table name: addresses
#
#  id            :uuid             not null, primary key
#  country_code  :string(2)        not null
#  division_code :string(6)        not null
#  city_locality :string(32)
#  postal_code   :string(16)
#  street        :string(64)
#
require 'rails_helper'

RSpec.describe Address, type: :model do
  subject { build(:address) }

  it { is_expected.to belong_to :country }

  it { is_expected.to respond_to :to_s }
end
