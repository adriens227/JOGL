# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'taggable' do
  include ActiveJob::TestHelper

  it { is_expected.to have_many(:tags) }
end
