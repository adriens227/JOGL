# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'optionable' do
  it 'uses default options' do
    expect(subject.options).not_to be_empty
  end
end
