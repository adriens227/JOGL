# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'visibility' do
  it 'sets a default visibility' do
    expect(subject.visibility).not_to be_nil
  end
end
