# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'mediable' do
  include ActiveJob::TestHelper
end
