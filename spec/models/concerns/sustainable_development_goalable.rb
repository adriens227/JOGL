# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'sustainable_development_goalable' do
  include ActiveJob::TestHelper

  it { is_expected.to have_many(:sustainable_development_goals) }
end
