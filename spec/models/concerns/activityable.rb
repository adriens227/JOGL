# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'activityable' do
  include ActiveJob::TestHelper

  it { is_expected.to have_many(:activities) }

  it 'returns the correct past activities' do
    create(
      :challenge,
      resource: subject,
      start: 3.days.ago,
      stop: 3.days.ago
    )
    create(
      :event,
      resource: subject,
      start: 3.days.from_now,
      stop: 3.days.from_now
    )

    subject.save!

    expect(subject.past_activities.count).to eq 1
  end

  it 'returns the correct upcoming activities' do
    create(
      :opportunity,
      resource: subject,
      start: 3.days.ago,
      stop: 3.days.ago
    )
    create(
      :peer_review,
      resource: subject,
      start: 3.days.from_now,
      stop: 3.days.from_now
    )

    subject.save!

    expect(subject.upcoming_activities.count).to eq 1
  end
end
