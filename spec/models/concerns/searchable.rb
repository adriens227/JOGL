# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'searchable' do
  include ActiveJob::TestHelper

  let(:model) { described_class }

  it 'has a search method' do
    expect(model).to respond_to(:search)
  end

  it 'reindexes after update' do
    subject.save!

    perform_enqueued_jobs

    field = model.search_fields.first
    content = FFaker::String.from_regexp(/[A-Za-z0-9]+/)

    subject.method("#{field}=").call(content)
    subject.save!

    perform_enqueued_jobs

    expect(model.search(content).execute.map(&:id)).to include(subject.id)
  end

  it 'deindexes before destroy' do
    subject.save!

    perform_enqueued_jobs

    field = model.search_fields.first
    content = subject.method(field).call[0..63]

    subject.destroy!

    perform_enqueued_jobs

    expect(model.search(content).execute.map(&:id)).not_to include(subject.id)
  end

  it 'truncates the index' do
    subject.save!

    model.index_truncate

    query = %(SELECT COUNT(*) FROM #{model.name.underscore.pluralize})

    expect(Search::CLIENT.query(query).first.values.first).to eq 0
  end
end
