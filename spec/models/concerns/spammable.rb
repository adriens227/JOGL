# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'spammable' do
  include ActiveJob::TestHelper

  before do
    stub_const('NOT_SPAM', 'foo bar baz')
  end

  let(:model) { described_class }

  it 'does not falsely identify spam' do
    field = model.spam_fields.first

    user = create(:user, description: NOT_SPAM, summary: NOT_SPAM)
    user.grant_role(:superadmin)

    instance = create(model.to_s.underscore.to_sym, field => NOT_SPAM)

    perform_enqueued_jobs

    notifications = Notification.where("params ->> 'id' = '#{instance.id}' AND params -> 'hits' @> '[\"#{field}\"]'")

    expect(notifications.count).to eq 0
  end

  it 'correctly identifies spam' do
    field = model.spam_fields.first

    user = create(:user, description: NOT_SPAM, summary: NOT_SPAM)
    user.grant_role(:superadmin)

    instance = create(model.to_s.underscore.to_sym, field => 'foo escort baz')

    perform_enqueued_jobs

    notifications = Notification.where("params ->> 'id' = '#{instance.id}' AND params -> 'hits' @> '[\"#{field}\"]'")

    expect(notifications.count).to eq 1
  end
end
