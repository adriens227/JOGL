# frozen_string_literal: true

require 'spec_helper'

shared_examples_for 'translateable' do
  include ActiveJob::TestHelper

  let(:model) { described_class }

  it 'creates a translation for every field and every language' do
    perform_enqueued_jobs

    expected = model.translated_fields.reduce(0) do |count, field|
      next count if subject.method(field).call.nil?

      count + Language.where(supported: true).count
    end

    actual = Translation.where(resource: subject).count

    expect(expected).to eq actual
  end

  it 'returns a translated version of the instance' do
    perform_enqueued_jobs

    language = Translation.where(edited: false, resource: subject).first.language_code

    translated = subject.translated(language)

    all = model.translated_fields.map do |field|
      next true if subject.method(field).call.nil?

      translation = Translation.find_by(edited: false, field: field, language_code: language, resource: subject)

      next false if translation.nil?

      translated.method(field).call == translation.content
    end.all?

    expect(all).to be true
  end
end
