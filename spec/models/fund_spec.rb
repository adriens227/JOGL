# frozen_string_literal: true

# == Schema Information
#
# Table name: funds
#
#  id                   :uuid             not null, primary key
#  resource_id          :uuid             not null
#  resource_type        :string           not null
#  total                :float            default(0.0), not null
#  maximum_disbursement :float
#  created_at           :datetime         not null
#  updated_at           :datetime         not null
#
require 'rails_helper'

RSpec.describe Fund, type: :model do
  subject { create(:fund) }

  it { is_expected.to belong_to(:resource) }

  it { is_expected.to validate_presence_of(:total) }
end
