# frozen_string_literal: true

# == Schema Information
#
# Table name: phases
#
#  id         :uuid             not null, primary key
#  project_id :uuid             not null
#  color      :string(6)        default("000000"), not null
#  deleted    :boolean          default(FALSE), not null
#  start      :date             not null
#  stop       :date             not null
#  title      :string(128)      not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
require 'rails_helper'

require Rails.root.join('spec/models/concerns/deletable.rb')
require Rails.root.join('spec/models/concerns/translateable.rb')

RSpec.describe Phase, type: :model do
  subject { create(:phase) }

  it_behaves_like 'deletable'
  it_behaves_like 'translateable'

  it { is_expected.to belong_to(:project) }

  it { is_expected.to have_many(:moments) }

  it { is_expected.to validate_presence_of(:start) }
  it { is_expected.to validate_presence_of(:stop) }
  it { is_expected.to validate_presence_of(:title) }
  it { is_expected.to validate_length_of(:title) }
end
