# frozen_string_literal: true

# == Schema Information
#
# Table name: countries
#
#  code            :string(2)        not null, primary key
#  name            :string(64)       not null
#  search_id       :bigint
#  divisions_count :integer          default(0), not null
#
require 'rails_helper'

RSpec.describe Country, type: :model do
  subject { described_class.first }

  it { is_expected.to have_many(:divisions) }

  it { is_expected.to validate_presence_of(:code) }
  it { is_expected.to validate_presence_of(:name) }

  it { is_expected.to validate_length_of(:code) }
  it { is_expected.to validate_length_of(:name) }
end
