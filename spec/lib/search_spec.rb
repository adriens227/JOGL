# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Search do
  include ActiveJob::TestHelper

  let(:title) { FFaker::CheesyLingo.title[0..127] }
  let(:search) { title.split.last }

  before do
    Program.index_truncate
    Project.index_truncate
    Space.index_truncate

    perform_enqueued_jobs

    create(:program, title: title)
    create(:project, title: title)
    create(:space, title: title)

    perform_enqueued_jobs
  end

  it 'can search within a single model' do
    results = Space.search(search).execute

    expect(results.count).to equal(1)
  end

  it 'can search using within' do
    results = described_class.new(search).within(Program, Project, Space).execute

    expect(results.count).to equal(3)
  end

  it 'can search using without' do
    results = described_class.new(search).within(Program, Project, Space).without(Project).execute

    expect(results.count).to equal(2)
  end

  it 'can search with simple attributes' do
    results = described_class.new(search).within(Space).attributes(category: 'other').execute

    expect(results.count).to equal(1)
  end

  it 'can search with array attributes' do
    results = described_class.new(search).within(Space).attributes(category: %w[other school]).execute

    expect(results.count).to equal(1)
  end

  it 'can search with range attributes' do
    past = 1.day.ago.to_i
    future = 1.day.from_now.to_i

    results = described_class.new(search).within(Space).attributes(created_at: past..future).execute

    expect(results.count).to equal(1)
  end

  it 'orders results' do
    results = described_class.new(search).within(Program, Project, Space).order('created_at ASC').execute

    instances = [Program.first, Project.first, Space.first].sort_by(&:created_at)

    expect(results.map(&:id)).to match_array(instances.map(&:id))
  end

  it 'limits the number of results' do
    results = described_class.new(search).within(Program, Project, Space).limit(1).execute

    expect(results.count).to equal(1)
  end

  it 'limits the results with an offset' do
    results = described_class.new(search).within(Program, Project, Space).limit(1, 1).execute

    control = described_class.new(search).within(Program, Project, Space).limit(1).execute

    expect(results.first.id).not_to equal(control.first.id)
  end
end
