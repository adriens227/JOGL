# frozen_string_literal: true

class StubApi < Sinatra::Base
  protected

  def json_response(response_code, file_name)
    content_type :json
    status response_code
    File.binread(File.join(File.dirname(__FILE__), "../fixtures/#{file_name}"))
  end
end
