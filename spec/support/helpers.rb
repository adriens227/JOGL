# frozen_string_literal: true

module Helpers
  def jwt
    { Authorization: "Bearer #{User.first.generate_token}" }
  end
end
