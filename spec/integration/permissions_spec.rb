# frozen_string_literal: true

require 'swagger_helper'

require Rails.root.join('spec/support/helpers.rb')

RSpec.describe 'users roles' do
  path '/permissions' do
    post 'index permissions' do
      tags 'permissions'

      consumes 'application/json'
      produces 'application/json'

      security [ApiKeyAuth: {}]

      parameter name: :params, in: :body, schema: {
        type: :object,
        properties: {
          id: { type: :string, format: :uuid },
          type: { type: :string }
        },
        required: %i[type]
      }

      response '200', 'success' do
        schema type: :array,
               items: { type: :string }

        let(:space) { create(:space) }
        let(:user) { create(:user, confirmed_at: Time.zone.now) }
        let(:'X-API-KEY') { user.key.id }
        let(:params) { { id: space.id, type: 'Space' } }

        run_test!

        after do |example|
          example.metadata[:response][:examples] =
            { 'application/json' => JSON.parse(response.body, symbolize_names: true) }
        end
      end
    end
  end
end
