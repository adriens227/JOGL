# frozen_string_literal: true

require 'swagger_helper'

require Rails.root.join('spec/support/helpers.rb')

RSpec.describe 'users roles' do
  path '/users/{user_id}/roles' do
    post 'create role' do
      tags 'users'

      consumes 'application/json'
      produces 'application/json'

      security [ApiKeyAuth: {}]

      parameter name: 'user_id', in: :path, type: :string, format: :uuid
      parameter name: 'role', in: :body, schema: { '$ref' => '#/components/schemas/role' }

      response '201', 'success' do
        let(:user) { create(:user, confirmed_at: Time.zone.now) }
        let(:'X-API-KEY') { user.key.id }
        let(:user_id) { user.id }
        let(:role) do
          { name: 'member', resource_id: create(:space, owner: user).id, resource_type: 'Space' }
        end

        run_test!
      end
    end

    delete 'delete role' do
      tags 'users'

      consumes 'application/json'
      produces 'application/json'

      security [ApiKeyAuth: {}]

      parameter name: 'user_id', in: :path, type: :string, format: :uuid
      parameter name: 'role', in: :body, schema: { '$ref' => '#/components/schemas/role' }

      response '200', 'success' do
        let(:user) { create(:user, confirmed_at: Time.zone.now) }
        let(:'X-API-KEY') { user.key.id }
        let(:user_id) { user.id }
        let(:role) do
          { name: 'member', resource_id: create(:space, owner: user).id, resource_type: 'Space' }
        end

        run_test!
      end
    end

    get 'index roles' do
      tags 'users'

      consumes 'application/json'
      produces 'application/json'

      security [ApiKeyAuth: {}]

      parameter name: 'user_id', in: :path, type: :string, format: :uuid

      response '200', 'success' do
        schema type: :array,
               items: {
                 '$ref' => '#/components/schemas/role'
               }

        let(:user) { create(:user, confirmed_at: Time.zone.now) }
        let(:'X-API-KEY') { user.key.id }
        let(:user_id) { user.id }

        run_test!
      end
    end
  end
end
